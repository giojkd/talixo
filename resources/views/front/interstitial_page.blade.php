@extends('front.index')

@section('header_jumbotron')
    <div class="common-page">
        <div class="jumbotron banner-head">
            <div class="container">

                @include('front/components/common_page_header')

            </div>
        </div> <!-- banner-head -->
    </div>
@endsection

@section('page_main')

    <div class="container">
        <div class="row my-4 py-4">
            <div class="col-md-12 text-center">
                <h1>Now that you have added {{$added_product->name}} to your trip...</h1>
                <div class="my-4">
                    Enrich your experience by continuing your trip with one of the following experiences...
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('front.components.products_grid')
            </div>
        </div>
    </div>

@endsection
