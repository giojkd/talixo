@extends('front.index')

@section('header_jumbotron')
<div class="common-page">
  <div class="jumbotron banner-head">
    <div class="container">

      @include('front/components/common_page_header')

    </div>
  </div> <!-- banner-head -->
</div>
@endsection
@section('page_main')

  <div class="container">
    <div class="col-md-12">
      <div class="pt-4 mt-4">
        <h1>{{$article->name}}</h1>
        {!!$article->content!!}
      </div>
    </div>
  </div>

@endsection
