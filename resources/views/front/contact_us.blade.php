@extends('front.index');

@section('header_jumbotron')
  <div class="common-page">
    <div class="jumbotron banner-head">
      <div class="container">

        @include('front/components/common_page_header')

      </div>
    </div> <!-- banner-head -->
  </div>
@endsection
@section('page_main')
  <div class="container">
    <div class="col-md-12">
      <div class="pt-4 mt-4">
        <h1>Contact us</h1>
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/shell.js"></script>
        <script>
        hbspt.forms.create({
          portalId: "7230374",
          formId: "288a9698-68c9-42fa-8e9e-9df449c8c229"
        });
        </script>

        <hr>

        <h3 class="mb-3">TALIXO TUSCANY S.R.L.</h3>
        <ul class="list-unstyled">
          <li class="mb-2">Piazza de' Frescobaldi 4, 50125 Firenze</li>
          <li class="mb-2"><a href="tel:+390550208165">Tel: 0550208165</a>
            <li class="mb-2"><a href="mailto:info@talixotuscany.com">eMail: info@talixotuscany.com</a>
              <li>Piva 06950670486</li>
        </ul>



      </div>
    </div>
  </div>
@endsection
