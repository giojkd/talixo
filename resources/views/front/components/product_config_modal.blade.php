<div class="modal fade" id="calendar-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add a new fantastic experience to your tour!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="col-sm-12 first-step-popup">
          <div class="row">
            <div class="add-exp-no"> <span> 1 </span></div>

            <div class="col-sm-10 add-what-time">
              <div class="row">
                <p class="sub-head col-sm-12"> When and what time ? </p>

                <div class="day-exp-book col-sm-4">
                  <h3>Day</h3>

                  <div class="calendar calendar-first" id="calendar_second">
                    <div class="calendar_header">
                      <button class="switch-month switch-left"> <i class="fa fa-toggle-left"></i></button>
                      <h2></h2>
                      <button class="switch-month switch-right"> <i class="fa fa-toggle-right"></i></button>
                    </div>
                    <div class="calendar_weekdays"></div>
                    <div class="calendar_content"></div>
                  </div>
                </div><!-- day-exp-book -->

                <div class="day-time-book  col-sm-4">
                  <h3>Time</h3>
                  <p>Between</p>
                  <div class="form-group">
                    <select class="form-control">
                      <option>9:30</option>
                    </select>
                  </div>

                  <p>and</p>
                  <div class="form-group">
                    <select class="form-control minimal">
                      <option>9:30</option>
                    </select>
                  </div>
                </div><!-- day-exp-book -->
              </div><!-- add-what-time -->
            </div>
          </div>
        </div><!-- col-sm-12 -->

        <div class="col-sm-12 second-step-popup">
          <div class="row">
            <div class="add-exp-no"> <span> 2 </span></div>

            <div class="col-sm-11 looking-for">
              <p class="sub-head"> What are you looking for ? </p>
              <ul>
                <li>
                  <div class="looking-img"><img src="images/dinning.png"></div>
                  <div class="looking-descp">
                    <h4>Dining</h4>
                    <p>Lorem ipsum dolor sit amet</p>
                  </div>
                </li>
                <li class="active">
                  <div class="looking-img"><img src="images/trekking.jpg"></div>
                  <div class="looking-descp">
                    <h4>Trekking</h4>
                    <p>Lorem ipsum dolor sit amet</p>
                  </div>
                </li>
                <li>
                  <div class="looking-img"><img src="images/dinning.png"></div>
                  <div class="looking-descp">
                    <h4>Dining</h4>
                    <p>Lorem ipsum dolor sit amet</p>
                  </div>
                </li>
                <li>
                  <div class="looking-img"><img src="images/trekking.jpg"></div>
                  <div class="looking-descp">
                    <h4>Trekking</h4>
                    <p>Lorem ipsum dolor sit amet</p>
                  </div>
                </li>
                <li>
                  <div class="looking-img"><img src="images/dinning.png"></div>
                  <div class="looking-descp">
                    <h4>Dining</h4>
                    <p>Lorem ipsum dolor sit amet</p>
                  </div>
                </li>
                <li>
                  <div class="looking-img"><img src="images/trekking.jpg"></div>
                  <div class="looking-descp">
                    <h4>Trekking</h4>
                    <p>Lorem ipsum dolor sit amet</p>
                  </div>
                </li>
              </ul>
            </div><!-- looing for -->
            <a class="load-more" href="#">Load more</a>

          </div>
        </div><!-- col-sm-12 second -->


        <div class="col-sm-12 third-step-popup">
          <div class="row">
            <div class="add-exp-no"> <span> 3 </span></div>

            <div class="col-sm-11 choose-fav">
              <p class="sub-head"> Choose your favourite among our selection of trekking experiences </p>
              <div class="col-sm-12 discover-experience-main">

                <div class="row">
                  <div class="col-sm-3 discover-inner">
                    <input class="choose-fav-checkbox" type="checkbox" checked>
                    <label class="choose-fav-label"></label>
                    <div class="discover-img">
                      <img src="images/travel.jpg">
                      <span class="sell-out">LIKELY TO SELL OUT</span>
                      <span class="like-heart"><img src="images/heart-icon.png"></span>
                      <div class="blue-area"> <span class="rate">8,8</span> <span class="avg">su 1050</span></div>
                    </div><!-- discover-img -->

                    <div class="discover-data">
                      <h3 class="location-name">Tour Culturale In Citta D'arte</h3>
                      <p>Scopri venezia insieme alla tua guida locale</p>
                      <p>Da <span class="travel-price">1999 <small>EUR</small></span> a persona</p>

                      <div class="map-duration">
                        <span class="map-loc">Venezia</span> <span class="jounery-duration">2.5 hours</span> <span class="star-rating">8,8(su1050)</span>
                      </div><!-- map-duration -->


                    </div><!-- discover-data -->
                  </div><!-- discover-inner -->

                  <div class="col-sm-3 discover-inner active">
                    <input class="choose-fav-checkbox" type="checkbox">
                    <label class="choose-fav-label"></label>
                    <div class="discover-img">
                      <img src="images/travel.jpg">
                      <span class="like-heart"><img src="images/heart-icon.png"></span>
                    </div><!-- discover-img -->

                    <div class="discover-data">
                      <h3 class="location-name">Tour Culturale In Citta D'arte</h3>
                      <p>Scopri venezia insieme alla tua guida locale</p>
                      <p>Da <span class="travel-price">1999 <small>EUR</small></span> a persona</p>

                      <div class="map-duration">
                        <span class="map-loc">Venezia</span> <span class="jounery-duration">2.5 hours</span> <span class="star-rating">8,8(su1050)</span>
                      </div><!-- map-duration -->


                    </div><!-- discover-data -->
                  </div><!-- discover-inner -->

                  <div class="col-sm-3 discover-inner">
                    <input class="choose-fav-checkbox" type="checkbox">
                    <label class="choose-fav-label"></label>
                    <div class="discover-img">
                      <img src="images/travel.jpg">
                      <span class="sell-out">LIKELY TO SELL OUT</span>
                      <span class="like-heart"><img src="images/heart-icon.png"></span>
                    </div><!-- discover-img -->

                    <div class="discover-data">
                      <h3 class="location-name">Tour Culturale In Citta D'arte</h3>
                      <p>Scopri venezia insieme alla tua guida locale</p>
                      <p>Da <span class="travel-price">1999 <small>EUR</small></span> a persona</p>

                      <div class="map-duration">
                        <span class="map-loc">Venezia</span> <span class="jounery-duration">2.5 hours</span> <span class="star-rating">8,8(su1050)</span>
                      </div><!-- map-duration -->


                    </div><!-- discover-data -->
                  </div><!-- discover-inner -->

                  <div class="col-sm-3 discover-inner">
                    <input class="choose-fav-checkbox" type="checkbox">
                    <label class="choose-fav-label"></label>
                    <div class="discover-img">
                      <img src="images/travel.jpg">
                      <span class="like-heart"><img src="images/heart-icon.png"></span>
                    </div><!-- discover-img -->

                    <div class="discover-data">
                      <h3 class="location-name">Tour Culturale In Citta D'arte</h3>
                      <p>Scopri venezia insieme alla tua guida locale</p>
                      <p>Da <span class="travel-price">1999 <small>EUR</small></span> a persona</p>

                      <div class="map-duration">
                        <span class="map-loc">Venezia</span> <span class="jounery-duration">2.5 hours</span> <span class="star-rating">8,8(su1050)</span>
                      </div><!-- map-duration -->


                    </div><!-- discover-data -->
                  </div><!-- discover-inner -->


                </div>
              </div><!-- discover-experience-main -->

            </div><!-- looing for -->

            <a class="load-more" href="#">Load more</a>

          </div>
        </div><!-- col-sm-12 third -->


        <div class="col-sm-12 fourth-step-popup">
          <div class="row">
            <div class="add-exp-no"> <span> 4 </span></div>

            <div class="col-sm-11 choose-time-option">

              <p class="sub-head"> Choose a time option </p>
              <div class="row">
                <div class="col-sm-4 white-block common-time-block">
                  <div class="row">
                    <div class="col-sm-5 time-check">
                      8:30
                      <a class="sell-out" href="#">likely to sell out</a>
                    </div>
                    <div class="col-sm-7 ruppe-check">
                      <p class="price">€ 195</p>
                      <p class="bottom-price"><span>Adulti </span>  €399,00 (€199,50 x 2 adulti)</p>
                      <p class="bottom-price"><span>Bambini </span> €399,00 (€199,50 x 2 adulti)</p>
                    </div>
                  </div>
                </div><!-- common-time-block -->

                <div class="col-sm-4 white-block common-time-block active">
                  <div class="row">
                    <div class="col-sm-5 time-check">
                      14:30
                    </div>
                    <div class="col-sm-7 ruppe-check">
                      <p class="price">€ 195</p>
                      <p class="bottom-price"><span>Adulti </span>  €399,00 (€199,50 x 2 adulti)</p>
                      <p class="bottom-price"><span>Bambini </span> €399,00 (€199,50 x 2 adulti)</p>
                    </div>
                  </div>
                </div><!-- common-time-block -->

              </div>
            </div><!-- choose-time-option -->
            <button class="place-order-btn">AGGIUNGI AL MIO TOUR</button>
          </div>


        </div><!-- col-sm-12 fourth -->


      </div><!--modal-body-->
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div><!-- modal -->
