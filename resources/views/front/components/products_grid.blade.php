<div class="col-sm-12 discover-experience-main">
  <div class="row">
    @foreach($products as $product)
    @if(isset($product->getPhotos()[0]))
    <a href="{{$product->getUrl('product')}}" class="col-sm-3 product-grid-element">
      <div class="discover-inner">
        <div class="discover-img-wrapper">


          <div class="discover-img" style="background:url('{{url($product->getCover())}}'); background-size: cover; background-position: center center;">


          </div><!-- discover-img -->
          @if($product->can_be_private)
            <span class="vip-label"> <i class="fa fa-star"></i> {{ucfirst(__('all.private tour'))}}</span>
          @endif
          <!--<span class="like-heart"><img src="http://localhost:8000/front/images/heart-icon.png"></span>-->
          <!--<div class="blue-area"> <span class="rate">8,8</span> <span class="avg">su 1050</span></div>-->
        </div>

        <div class="discover-data">
          <h3 class="location-name">{{Str::limit($product->name,28,$end='...')}}</h3>
          <p>{{Str::limit($product->short_description, 60, $end='...')}}</p>

          @if($product->availabilities->count() > 0)
          <p>From <span class="travel-price">{{$product->availabilities->min('price_per_adult')}} <small>EUR</small></span> {{ucfirst(__('all.per person'))}}</p>
          @endif

          <div class="map-duration">
            <span class="map-loc">{{$product->geofence->name}}</span>
            <span class="jounery-duration">{{Str::limit($product['duration'],5,$end='')}} {{ucfirst(__('all.hours'))}}</span>
            <!--<span class="star-rating">8,8(su1050)</span>-->
          </div><!-- map-duration -->

          @if($product->availabilities->count() > 0)
          <div class="calendar-checkin-checkout">
            @foreach($product->availabilities as $index => $availability)
            @if($index < 2)
            <span>{{$availability->day->format('D d')}} {{Str::limit($availability['time'],5,$end='')}}</span>
            @endif
            @endforeach

            @if(($product->availabilities->count()-2) > 0)
            <br>
            <span class="more-option">+ {{$product->availabilities->count()-2}} {{ucfirst(__('all.more'))}}</span>
            @endif
          </div>
          <div class="clearfix">

          </div>
          @endif
        </div><!-- discover-data -->
      </div>
    </a><!-- discover-inner -->
    @endif
    @endforeach

    <!--<p class="grey-btn-area-para"><a class="grey-btn" href="#">Scopri altre 300 esperienze 	 <span> &gt; </span></a></p>-->

  </div>
</div>
<div class="clearfix">

</div>
