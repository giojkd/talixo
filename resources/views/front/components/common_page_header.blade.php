<header>
  <div class="logo">
    <a href="{{route('home')}}">
      <img src="{{asset('front/images/talixo-logo.png')}}">
    </a>
  </div>
  <!--
  <div class="search-bar">
    <div class="form-group search-web">
    <input type="text" class="form-control" placeholder="Cerca"> </div>
  </div>-->
  <div class="menu-bar">
    <div class="container-fluid">

      <nav class="navbar navbar-expand-sm  navbar-dark">
        <!-- <a class="navbar-brand" href="#">Navbar</a> -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
          <ul class="navbar-nav">
              <li class="nav-item">
                  <a href="{{route('vouchers')}}" class="nav-link special-link">
                      <i class="fa fa-gift"></i> {{ucfirst(__('all.gift'))}}
                  </a>
              </li>
            @foreach($menus['top_menu']->articles as $article)
            <li class="nav-item">
              <a class="nav-link" href="{{$article->getUrl('article')}}">{{$article->name}}</a>
            </li>
            @endforeach
            <li class="nav-item">
              <a class="nav-link" href="{{route('contactUs')}}">{{ucfirst(__('all.contact us'))}}</a>
            </li>
            <li class="nav-item">
              <a href="{{route('experiences')}}" class="nav-link special-link">
                <i class="fa fa-star"></i> {{ucfirst(__('all.experiences'))}}
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('map')}}" class="nav-link special-link">
                <i class="fa fa-glass"></i> {{ucfirst(__('all.beyond the label'))}}
              </a>
            </li>


            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{substr($languages[app()->getLocale()]->name,0,3)}}
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                @foreach($languages as $index => $language)
                  @if($index != app()->getLocale())
                    <a href="{{route('languageSwitcher',['language' => $language->iso])}}" class="dropdown-item" >{{substr($language->name,0,3)}}</a>
                  @endif
                @endforeach
              </div>
            </li>
            <!--
            <li class="country-symbol">
              <span class="currency">€</span>
              <span class="flag">
              <img src="images/country.jpg"></span>
            </li>
            <li class="login-registration">
              <a href="#">Accedi</a>
              <a href="#">Registrati</a>
            </li>
          -->
            </ul>
          </div>
        </nav>
      </div>
    </div>
  </header>
