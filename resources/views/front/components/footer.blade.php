<div class="jumbotron footer-banner">
  <div class="container">

    <footer>
      <div class="col-md-12">
        <div class="row">
          <div class="col-sm-3 footer-first-col">
            <div class="logo-footer"><img src="{{asset('front/images/talixo-logo.png')}}"></div>
            <h3> TALIXO TUSCANY </h3>
            <p>
              Talixo Tuscany srl<br>
              Piazza de Frescobaldi, 4<br>
              50124 Firenze<br>
              Piva 062342394871<br>
            </p>
            <div class="social-footer">
              <ul>
                <!--<li><a class="fa fa-twitter" href="#"></a></li>-->
                <!--<li><a class="fa fa-pinterest" href="#"></a></li>-->
                <li><a class="fa fa-facebook" href="https://www.facebook.com/talixotuscany/"></a></li>
                <li><a class="fa fa-instagram" href="https://www.instagram.com/talixotuscany/"></a></li>
              </ul>
            </div>
          </div><!-- footer-first-col -->

          <div class="col-sm-3 footer-second-col">
            <div class="footer-links">
              <ul>
                @foreach($menus['footer_menu_1']->articles as $article)
                <li><a href="{{$article->getUrl('article')}}">{{$article->name}}</a></li>
                @endforeach
              </ul>
              <!--<button class="red-btn-footer">Suggeriscici un’esperienza</button>-->
            </div>
          </div><!-- footer-second-col -->

          <div class="col-sm-3 footer-second-col">
            <div class="footer-links">
              <ul>
                @foreach($menus['footer_menu_2']->articles as $article)
                <li><a href="{{$article->getUrl('article')}}">{{$article->name}}</a></li>
                @endforeach
              </ul>
              <!--<button class="red-btn-footer">Lavora con noi</button>-->
            </div>
          </div><!-- footer-third-col -->


          <div class="col-sm-3 footer-fourth-col">
            <div class="subscribe-newsletter">
              <h2>SUBSCRIBE OUR NEWSLETTER</h2>
              <p>And stay up to date on our fantastic deals!</p>

              <div class="subscribe-form">
                <form method="GET" action="{{route('newsletterSubscription')}}">
                  <div class="form-group email">
                    <input name="email" type="text" class="form-control" placeholder="Your email address" required> </div>

                    <div class="form-group calendar">
                      <div class="input-group date" id="dob">
                        <input autocomplete="new-password" name="date_of_birth" type="text" class="form-control" placeholder="Date of birth">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                      </div>
                    </div><!-- calendar -->


                    <div class="form-group nationality">
                      <div class="input-group date" >
                        <input autocomplete="new-password" name="nationality" type="text" class="form-control" placeholder="Nationality">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                      </div>
                    </div>

                    <div class="form-group gender">
                      <select name="gender" class="form-control">
                        <option>Gender</option>
                        <option>Male</option>
                        <option>Female</option>
                        <option>Prefer not to declare</option>
                      </select>
                    </div><!-- gender -->

                    <div class="checkbox">
                      <input type="checkbox" required>
                      <label>by submitting this form I agree to the terms and conditions</label>
                    </div>
                    <input type="submit" value="Subscribe now">
                  </form>
                </div><!-- subscribe-form -->
              </div><!-- subscribe-newsletter -->
            </div><!-- footer-fourth-col -->


          </div>
        </div>
      </footer>

    </div><!-- container -->
    {{-- <script id="CookieDeclaration" src="https://consent.cookiebot.com/4daebedc-8e43-4e47-99f6-a26249f2bb81/cd.js" type="text/javascript" async></script>  --}}
  </div> <!-- footer -->
