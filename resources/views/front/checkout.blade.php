
@extends('front.index')

@section('header_jumbotron')
<div class="common-page">
  <div class="jumbotron banner-head">
    <div class="container">

      @include('front/components/common_page_header')

    </div>
  </div> <!-- banner-head -->
</div>
@endsection

@section('page_main')

<div class="container">
  @if($rows->count() > 0)
  <div class="col-sm-12 checkout-details">
    <div class="row">
      <div class="col-sm-5 left-main">

        <h2>{{ucfirst(__('all.confirm your tour'))}}!</h2>
        <p>
          {{ucfirst(__('all.tell us where you want us to come to pick you up'))}}
        </p>

        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif


        <form id="pickupAddressForm" action="{{route('calculateTransferCost')}}" method="post">
          @csrf

          <input type="hidden" name="pickup_address_latitude" value="{{$lead->pickup_address_latitude}}">
          <input type="hidden" name="pickup_address_longitude" value="{{$lead->pickup_address_longitude}}">

          <div class="checkout-login mb-4">
            <div class="row">
              <div class="col-sm-12"><h3>1 - {{ucfirst(__('all.pick up location'))}}</h3></div>
              <div class="form-group  col-sm-9 dove-loc">
                <input id="pickup_address" value="{{$lead->pickup_address}}" required name="pickup_address"  type="text" class="form-control" placeholder="Via Roma 1, 50125 Firenze">
              </div>
              <div class="col-sm-3 px-0">
                <button type="submit" class="place-order-btn mt-0" name="button">{{ucfirst(__('all.confirm'))}}</button>
              </div>

              <!--<div class="form-group select-country  col-sm-6">
              <select name="pickup_vehicle" class="form-control">
              <option>Mercedes Van</option>
            </select>
          </div>-->
        </div>
      </div><!-- checkout-login -->
      @if($lead->transfer_price > 0)
      <div class="talixo-alert">
        <div class="row">

          <div class="col-5">
            <h1>€ {{$lead->transfer_price}}</h1>
          </div>
          <div class="col-7">

              {{ucfirst(__('all.price increase due to transfer lenght'))}} ({{number_format($lead->transfer_km,2)}}km and {{substr($lead->transfer_disposal,0,5)}} hours of driver's time at your full disposal)
          </div>
        </div>
      </div>
      @endif
    </form>

    <!--<div class="caution col-sm-12"></div>-->

    <form id="pay-form" method="post" action="{{route('pay')}}">
      @csrf
      @if($lead->pickup_address != '')
      <input value="{{$lead->pickup_address}}" name="pickup_address"  type="hidden">
      @endif
      <div class="checkout-second-step">
        <div class="row">
          <div class="col-sm-12">
            <h3>2 - {{ucfirst(__('all.billing address'))}} </h3><p>{{ucfirst(__('all.all the fields are required'))}}</p></div>
            <div class="form-group  col-sm-6"> <input value="{{old('client_name')}}" required name="client_name" type="text" class="form-control" placeholder="{{ucfirst(__('all.name'))}}"> </div>
            <div class="form-group  col-sm-6"> <input value="{{old('client_surname')}}" required name="client_surname" type="text" class="form-control" placeholder="{{ucfirst(__('all.surname'))}}"> </div>

            <div class="form-group  col-sm-6"> <input value="{{old('client_email')}}" required name="client_email" type="text" class="form-control" placeholder="Email"> </div>
            <div class="form-group  col-sm-6"> <input value="{{old('client_telephone')}}" required name="client_telephone" type="text" class="form-control" placeholder="{{ucfirst(__('all.tellephone'))}}"> </div>

            <div class="form-group  col-sm-12"> <input id="billing_address" value="{{old('client_address')}}" required name="client_address" type="text" class="form-control" placeholder="{{ucfirst(__('all.address'))}}"> </div>

            <div class="form-group  col-sm-12"> <textarea value="{{old('note')}}" name="notes" placeholder="{{ucfirst(__('all.allergies, diary restrictions or any other request'))}}"></textarea> </div>
          </div>
        </div><!-- checkout-second-step -->


        <div class="checkout-third-step">
          <div class="row">
            <div class="col-sm-12">{{ucfirst(__('all.the total will be billed only after the tour has been confirmed'))}}
              <h3>3 - {{ucfirst(__('all.payment'))}}</h3><p></p>

              <div class="radio-payment">
                <div class="radio credit-payment">
                  <div class="radio-left col-sm-2">
                    <input name="payment_method" value="credit_card" type="radio" checked>
                  </div>
                  <div class="radio-right  col-sm-10">
                    <p class="pl-0">{{ucfirst(__('all.pay with credit card'))}} <img src="{{asset('front/images/visa.png')}}"></p>
                    <div class="form-group  col-sm-12"> <input name="card[num]" type="text" class="form-control" placeholder="{{ucfirst(__('all.credit card number'))}}"> </div>
                    <div class="form-group  col-sm-12"> <input name="card[name]" type="text" class="form-control" placeholder="{{ucfirst(__('all.cardholder name'))}}"> </div>
                    <div class="form-group  col-sm-12">
                      <div class="row">
                        <div class="col-sm-4">
                          <input name="card[mm]" type="text" class="form-control" placeholder="{{ucfirst(__('all.exp'))}}. mm">
                        </div>
                        <div class="col-sm-4">
                          <input name="card[yy]" type="text" class="form-control" placeholder="{{ucfirst(__('all.exp'))}}. yy">
                        </div>
                        <div class="col-sm-4">
                          <input name="card[ccv]" type="text" class="form-control" placeholder="CCV">
                        </div>
                      </div>
                    </div>
                  </div>
                </div><!-- radio -->

                <!--
                <div class="radio">
                <div class="radio-left col-sm-2">
                <input name="payment_method" value="paypal" type="radio">
              </div>
              <div class="radio-right  col-sm-10">
              <p>Pay with con PayPal <img src="images/paypal.png"></p>
            </div>
          </div>

          <div class="radio">
          <div class="radio-left  col-sm-2">
          <input name="payment_method" value="bank_wire_transfer" type="radio">
        </div>
        <div class="radio-right  col-sm-10">
        <p>Pay with bank wire transfer <img src="images/bonifico.png"></p>
      </div>
    </div>

  -->

</div>
</div><!-- radio-payment-->

</div>
</div><!-- checkout-second-step -->
</form>
</div><!-- col-sm-4 left-main -->


<div class="col-sm-7 right-main stick-to-top vh-100 pt-4">

  @foreach($rows as $index =>  $row)
  <div class="col-sm-12 fleft ">
    <div class="col-sm-4 day">
      <!--<h2>Day 1</h2>-->
      <div class="pos-top">
        <div class="date-time-min">
          <p>{{$row->date->format('D d')}}<br>{{$row->date->format('F Y')}}</p>
          <p><span>{{Str::limit($row->time,5,$end='')}}</span></p>
          <!--<p>30 min.</p>-->

          <div class="day-counter"><span>{{$index+1}}</span></div>
        </div>
      </div>
    </div> <!-- col-sm-4 day -->

    <div class="col-sm-8 pro-desc">
      <div class="pro-desc-inner">
        <a class="close-pro" href="{{route('removeFromCart',['id' => $row->id])}}"></a>
        <!--<div class="pro-pic-inner">
        <img src="images/pro-gallery3.png">
      </div>-->
      <div class="venue-details">
        <div class="venue-duration">
          <!--<span class="service-hotel">Exclusive Dining</span>-->
          <span class="duration-hotel">{{ucfirst(__('all.duration'))}}: {{Str::limit($row->product['duration'],5,$end='')}} {{ucfirst(__('all.hours'))}}</span>
        </div><!-- venue-duration -->
        <div class="venue-title">
          <h3>{{$row->product->name}}</h3>
        </div><!-- venue-title -->

        <div class="venue-desc mb-0">
          <p>{{$row->product->short_description}}</p>
          @if($row->addons->count() > 0)
          {{ucfirst(__('all.includes'))}}
            <div class="checkout-addons">
              @foreach($row->addons as $addon)
                  <div class="checkout-addon">
                      {{$addon->name}}
                  </div>
              @endforeach
            </div>

          @endif
          <p class="mb-0 pb-0">{{$row->adults}} {{ucfirst(__('all.adults'))}} @if($row->children > 0) {{ucfirst(__('all.and'))}} {{$row->children}} {{ucfirst(__('all.children'))}} @endif <b>Tot: € {{$row->total}}</b></p>
        </div><!-- venue-desc -->

      </div><!-- venue-details -->

    </div><!-- pro-desc-inner -->
  </div> <!-- col-sm-8 day -->
</div><!-- Day one col-sm-12 fleft -->
@endforeach


<div class="col-sm-12 fleft">
  <div class="row">
    <div class="price-section">
      <!--<h2>Riepilogo</h2>
      <table cellpadding="0" cellspacing="0" align="center" width="100%">
      <tr>
      <td class="watch left-align"><img src="images/watch.png"> Orario</td>
      <td class="center-align">8:30</td>
      <td class="right-align">Adulti </td>
      <td>€399,00 (€199,50 x 2 adulti) </td>
    </tr>
    <tr>
    <td class="public left-align"><img src="images/public.png"> Adulti <span>(€199,50 per adulto)</span></td>
    <td class="center-align">2</td>
    <td class="right-align">Bambini  </td>
    <td>€399,00 (€199,50 x 2 adulti)</td>
  </tr>
  <tr>
  <td class="person left-align"><img src="images/person.png"> Bambini <span>(€199,50 per bambino)</span></td>
  <td class="center-align">2</td>
  <td class="right-align">Totale </td>
  <td class="grand-total">€ {{$lead->grand_total}}</td>
</tr>
</table>-->
<div class="form-group mb-0">
    <label class="mb-0"><input value="1" name="accept_terms_and_conditions" type="checkbox"> @lang('all.accept terms and conditions')</label>
</div>
<button onclick="$('#pay-form').submit()" type="button" class="place-order-btn">{{ucfirst(__('all.pay now'))}} € {{$lead->grand_total}}</button>
</div>
</div>
</div>


</div><!-- col-sm-8 right-main -->

</div>
</div><!-- col-sm-12  product-details -->
@else
<div class="alert alert-dark mt-4" role="alert">
  You didn't configure your tour yet! What are you waiting for? <a href="{{route('experiences')}}">Go now!</a>
</div>
@endif

</div><!-- container -->

@endsection


@push('footer_scripts')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API_KEY')}}&libraries=places"></script>
<script type="text/javascript">
var input_billing = document.getElementById('billing_address');
var autocomplete_billing = new google.maps.places.Autocomplete(input_billing);

var input = document.getElementById('pickup_address');
var autocomplete = new google.maps.places.Autocomplete(input,{
    componentRestrictions: {country: "it"}
});
autocomplete.addListener('place_changed', function() {
  var place = autocomplete.getPlace();

  $('input[name="pickup_address_latitude"]').val(place.geometry.location.lat())
  $('input[name="pickup_address_longitude"]').val(place.geometry.location.lng())
})
</script>

@endpush
