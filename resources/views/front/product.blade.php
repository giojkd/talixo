@extends('front.index')

@section('header_jumbotron')
<div class="common-page">
  <div class="jumbotron banner-head">
    <div class="container">

      @include('front/components/common_page_header')

    </div>
  </div>
</div>
<div class="jumbotron product-banner" id="photos-section">
  <div class="row">
    <div class="col-sm-6">
      <div class="img-holder" style="background-image:url(/{{$product->getPhotos()[0]}})"></div>
    </div>
    <div class="col-sm-3">

      <div class="img-holder" style="background-image:url(/{{$product->getPhotos()[1]}})"></div>
      <div style="height: 4px"></div>
      <div class="img-holder" style="background-image:url(/{{$product->getPhotos()[2]}})"></div>
    </div>
    <div class="col-sm-3">
      <div class="img-holder" style="background-image:url(/{{$product->getPhotos()[3]}})"></div>
      <div style="height: 4px"></div>
      <div class="img-holder" style="background-image:url(/{{$product->getPhotos()[4]}})"></div>
    </div>
    <!--<a class="grey-btn" href="#">Guarda tutte le foto	 <span> &gt; </span></a>-->
  </div>
</div>
<div class="jumbotron black-banner">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 event-name">
        <h2>{{$product->name}}</h2>
        <p class="caption">{{$product->subtitle}}</p>
        <p class="country">{{$product->geofence->name}}</p>
      </div>

      <div class="col-sm-6">
        <ul class="inclusions">
          <li>
            <img src="{{asset('front/images/event-duration.png')}}">
            <p><span>{{ucfirst(__('all.duration'))}}</span></p>
            <p>{{Str::limit($product['duration'],5,$end='')}} hours</p>
          </li>
          <li>
            <img src="{{asset('front/images/event-location-world.png')}}">
            <p>
              <span>{{ucfirst(__('all.available in'))}}</span>
            </p>
            @foreach($product->languages as $language)
            <p>{{$language->name}}</p>
            @endforeach
          </li>
          <!--<li>
          <img src="images/event-location-world.png">
          <p><span>Main locations</span></p>
          <p>Italian <br>English</p>
        </li>
        <li>
        <img src="images/event-direction.png">
        <p><span>Main locations</span></p>
        <p>Italian <br>English</p>
      </li>-->
    </ul>
  </div>
</div>
</div>
</div>
@endsection

@section('page_main')

<div class="breadcrum stick-to-top">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 breadcrum-inner">
        <ul>
          <li><a class="active" href="javascript:" onclick="scrollToDiv('body')">{{ucfirst(__('all.photos'))}}</a></li>
          <li><a href="javascript:" onclick="scrollToDiv('#description-section')">{{ucfirst(__('all.description'))}}</a></li>
          @if($product->addons->count() > 0)
            <li ><a href="javascript:" onclick="scrollToDiv('#addons-section')">{{ucfirst(__('all.enrich your experience'))}}</a></li>
          @endif
          @if($product->reviews->count()>0)
            <li ><a href="javascript:" onclick="scrollToDiv('#reviews-section')">{{ucfirst(__('all.reviews'))}}</a></li>
          @endif

          @if($product->whats_not_included != '')
            <li><a href="javascript:" onclick="scrollToDiv('#you-should-know-section')">{{ucfirst(__('all.you should know'))}}...</a></li>
          @endif
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="col-sm-12 product-details">
    <div class="row">
      <div class="col-sm-8 left-main">
        <h2 id="description-section">{{ucfirst(__('all.description'))}}</h2>
        <div class="product-description my-4">
          {!!$product->description!!}
        </div>

        @if($product->addons->count() > 0)
        <h2 id="addons-section">{{ucfirst(__('all.enrich your experience'))}}</h2>

        <div id="addonsCarousel" class="carousel slide my-4" data-ride="carousel">
          <div class="carousel-inner">
            @foreach($product->addons->chunk(3) as $index =>  $chunk)
            <div class="carousel-item @if($index == 0) active @endif">
              <div class="row">
                @foreach($chunk as $addon)
                <div class="col-md-4">
                  <div class="addon">
                    @if(isset($addon->cover))
                        <div class="cover" style="background-image:url('{{url($addon->cover)}}')">

                        </div>
                    @endif
                    <div class="content">
                      <h3>{{$addon->name}}</h3>
                      <p>{!!$addon->description!!}</p>
                      <p class="price-info">
                        <img src="{{asset('front/images/public.png')}}" alt=""> € {{$addon->price_per_adult}} {{__('all.per adult')}} (Tot:<span id="tot_add_on_{{$addon->id}}_adults">0,00</span>) <br>
                        <img src="{{asset('front/images/person.png')}}" alt=""> € {{$addon->price_per_child}} {{__('all.per child')}} (Tot:<span id="tot_add_on_{{$addon->id}}_children">0,00</span>)
                      </p>

                      <div class="btn-group-toggle addon-toggle-btn" data-toggle="buttons">
                        <label class="btn btn-addRemoveAddon btn-block">
                          <input data-id="{{$addon->id}}" data-price_per_adult="{{$addon->price_per_adult}}" data-price_per_child="{{$addon->price_per_child}}" class="checkbox-addon" type="checkbox" > <span class="add">{{ucfirst(__('all.add to cart'))}}</span> <span class="remove">{{ucfirst(__('all.remove from cart'))}}</span>
                        </label>
                      </div>

                    </div>
                  </div>

                </div>
                @endforeach
              </div>
            </div>
            @endforeach

          </div>
          @if($product->addons->count() > 1)
          <a class="carousel-control-prev" href="#addonsCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#addonsCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
          @endif
        </div>

        @endif

        <h2 id="you-should-know-section">{{ucfirst(__('all.what is included'))}}</h2>
        <div class="my-4">
          {!!$product->whats_included!!}
        </div>
        <br>
        @if($product->whats_not_included != '')
        <h2>{{ucfirst(__('all.your should know'))}}...</h2>
        <div class="my-4">
          {!!$product->whats_not_included!!}
        </div>


        @endif
        <br>




        <!--
        <div class="col-sm-12 fleft ">
        <div class="col-sm-4 day">
        <h2>Day 1</h2>
        <div class="pos-top">
        <div class="date-time-min">
        <p>Wed 25th Dec 19</p>
        <p><span>09.30</span> am</p>
        <p>30 min.</p>

        <div class="day-counter"><span>1</span></div>
      </div>
    </div>
    <button class="add-activity">Add activity</button>
  </div>

  <div class="col-sm-8 pro-desc">
  <div class="pro-desc-inner">
  <a class="close-pro" href=""></a>
  <div class="pro-pic-inner">
  <img src="images/pro-gallery3.png">
</div>

<div class="venue-details">
<div class="venue-duration">
<span class="service-hotel">Exclusive Dining</span>
<span class="duration-hotel">Duration: 2 hours 40 minutes </span>
</div>
<div class="venue-title">
<h3>Cena in Fabrica</h3>
</div>

<div class="venue-desc">
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecen as sodales imperdiet elementum.</p>
</div>

<div class="venue-action">
<a class="venue-search" href="#">Mostra dettagli</a>
<a class="venue-modify" data-toggle="modal" data-target="#calendar-popup" href="">Modifica</a>
</div>
</div>

</div>
</div>
</div>

-->


<!--

<div class="col-sm-12 fleft ">
<div class="col-sm-4 day">
<div class="pos-bottom">
<div class="date-time-min">
<p>Wed 25th Dec 19</p>
<p><span>11.30</span> am</p>
<p>30 min.</p>

<div class="day-counter"> <span>2</span></div>
</div>
</div>
</div>

<div class="col-sm-8 pro-desc">
<div class="pro-desc-inner">
<a class="close-pro" href=""></a>
<div class="pro-pic-inner">
<img src="images/pro-gallery3.png">
</div>

<div class="venue-details">
<div class="venue-duration">
<span class="service-hotel">Exclusive Dining</span>
<span class="duration-hotel">Duration: 2 hours 40 minutes </span>
</div>
<div class="venue-title">
<h3>Cena in Fabrica</h3>
</div>

<div class="venue-desc">
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecen as sodales imperdiet elementum.</p>
</div>

<div class="venue-action">
<a class="venue-search" href="#">Mostra dettagli</a>
<a class="venue-modify" href="#">Modifica</a>
</div>
</div>

</div>
</div>
</div>
-->



</div><!-- col-sm-8 left-main -->

<div class="col-sm-4 right-main product-page">
  <div class="calendar_product sticky-top">
    <div class="calendar-top-area col-sm-12">
      <div class="calendar-top-area-left  col-sm-7">
        <h2>{{$product->name}}</h2>
        <p>{{$product->subtitle}}</p>
      </div><!-- calendar-top-area-left -->

      <div class="calendar-top-area-right  col-sm-5">
        @if($product->reviews->count()>0)
        <span>{{number_format($product->reviews->avg('evaluation'),1)}}</span>
        {{__('all.on')}} {{$product->reviews->count()}} {{__('all.reviews')}}
        @endif
      </div><!-- calendar-top-area-right -->
    </div><!-- calendar-top-area -->

    <div class="calendar-main-area col-sm-12">

      <div class="calendar calendar-first" id="calendar_first">

      </div>

    </div><!-- calendar-main-area -->

    <div class="calendar-time-area col-sm-12" style="margin-bottom: 30px;">
      <div class="time-zone"> <span><img src="{{asset('front/images/watch.png')}}"> {{ucfirst(__('all.what time'))}}</span>
        <div class="select-time-zone">
          <select class="form-control" id="available_times">
            <option>0:00</option>
          </select>
        </div>
      </div><!-- time-zone -->


      <div class="time-zone adult-zone">
        <span><img src="{{asset('front/images/public.png')}}"> {{ucfirst(__('all.adults'))}} <small id="price_per_adult_show"> (0,00 {{__('all.per adult')}})</small></span>

        <div class="input-group">
          <span class="input-group-btn">
            <button type="button" class="btn-number red-btn-round plus-minus"  data-type="minus" data-field="quant[1]">
              <span class="glyphicon glyphicon-minus">-</span>
            </button>
          </span>
          <input id="adults_num" type="text" name="quant[1]" class="form-control input-number" value="2" min="1" max="100">
          <span class="input-group-btn">
            <button type="button" class=" btn-number red-btn-round plus-minus" data-type="plus" data-field="quant[1]">
              <span class="glyphicon glyphicon-plus">+</span>
            </button>
          </span>
        </div><!-- input-group -->

      </div><!-- adult-zone -->


      <div class="time-zone child-zone">
        <span><img src="{{asset('front/images/person.png')}}"> {{ucfirst(__('all.children'))}} <small id="price_per_child_show"> (0,00 {{__('all.per child')}})</small></span>

        <div class="input-group">
          <span class="input-group-btn">
            <button type="button" class="btn-number red-btn-round plus-minus"  data-type="minus" data-field="quant[2]">
              <span class="glyphicon glyphicon-minus">-</span>
            </button>
          </span>
          <input id="children_num" type="text" name="quant[2]" class="form-control input-number" value="0" min="0" max="100">
          <span class="input-group-btn">
            <button type="button" class=" btn-number red-btn-round plus-minus" data-type="plus" data-field="quant[2]">
              <span class="glyphicon glyphicon-plus">+</span>
            </button>
          </span>
        </div><!-- input-group -->


      </div><!-- child-zone -->

    </div><!-- calendar-main-area -->

    <div class="calendar-price-table  col-sm-12">
      <table cellpadding="0" cellspacing="0" align="center" width="100%">
        <tbody>
          <tr>
            <td class="left-align">{{ucfirst(__('all.adults'))}} </td>
            <td id="adults_price_summary">€0,00 (€0,00 x 0 {{__('all.adults')}}) </td>
          </tr>
          <tr>
            <td class="left-align">{{ucfirst(__('all.children'))}}  </td>
            <td id="children_price_summary">€0,00 (€0,00 x 0 {{__('all.children')}})</td>
          </tr>
          <tr>
            <td class="left-align">{{ucfirst(__('all.total'))}} </td>
            <td class="grand-total">€ 0,00 </td>
          </tr>
        </tbody>
      </table>
      <form action="{{route('addToCart')}}" method="post">
        @csrf
        <input type="hidden" name="product_id" value="{{$product->id}}">
        <input id="input_adults" type="hidden" name="adults" value="">
        <input id="input_children" type="hidden" name="children" value="">
        <input id="input_date" type="hidden" name="date" value="">
        <input id="input_time" type="hidden" name="time" value="">
        <input id="input_addons" type="hidden" name="input_addons" value="">
        <button class="place-order-btn" type="submit">{{ucfirst(__('all.book now'))}}</button>
      </form>

      <!--<a href="#" class="red-link">Oppure prenota per un gruppo privato</a>-->
    </div><!-- calendar-price-table -->
    <!--
    <div class="calendar-social-sharing  col-sm-12">
    <div class="row">
    <div class="calendar-social col-sm-8">
    <p>Condividi con</p>
    <ul class="Condividi">
    <li><a class="fa fa-twitter" href=""></a></li>
    <li><a class="fa fa-pinterest" href=""></a></li>
    <li><a class="fa fa-facebook" href=""></a></li>
    <li><a class="fa fa-instagram" href=""></a></li>
  </ul>
</div>

<div class="calendar-heart col-sm-4">
<p>Salva</p>
<img src="images/heart-icon.png">
</div>
</div>
</div>-->

</div><!-- calendar -->
</div><!-- col-sm-4 right-main -->

</div>
</div><!-- col-sm-12  product-details -->

<!--
<div class="col-sm-12 map">
<div class="map-inner">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11797.358592376035!2d12.290261880038843!3d42.33528147895237!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132f3ae1b413fab1%3A0x5980587f169a49d7!2s01034%20Fabrica%20di%20Roma%20VT%2C%20Italy!5e0!3m2!1sen!2sin!4v1578892365283!5m2!1sen!2sin" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</div>
</div>
-->

<div class="col-sm-12  rating-block">
  @if($product->reviews->count()>0)
  <h2 id="reviews-section">{{ucfirst(__('all.rated'))}} <img src="{{asset('front/images/ratings.png')}}"> {{__('all.on')}} {{$product->reviews->count()}} {{__('all.reviews')}}</h2>
  @foreach($product->reviews as $review)
  <div class="col-sm-10 customer-review">
    <div class="row">
      <div class="col-sm-1"> <img src="/{{$review->avatar}}"> </div>
      <div class="col-sm-11">
        <div class="raring-star"></div>
        <div class="pro-title">{{$review->alias}} <span class="country-flag">{{$review->nationality}}</div><!-- pro-title -->
          <div class="pro-date">{{$review->created_at->format('F d, Y')}} </div><!-- pro-date -->
        </div>

        <div class="col-sm-12">
          <div class="row">
            <div class="col-sm-1"><span class="like-text">{{ucfirst(__('all.i liked'))}}</span></div>
            <div class="col-sm-10">{{$review->liked}}</div>
          </div>
        </div>
        @if($review->not_liked != '')
        <div class="col-sm-12">
          <div class="row">
            <div class="col-sm-1"><span class="un-like-text">{{ucfirst(__('all.i did not like'))}}</span></div>
            <div class="col-sm-10">{{$review->not_liked}}</div>
          </div>
        </div>
        @endif
      </div>
    </div><!-- customer-review -->
    @endforeach




  </div><!-- rating-block -->

  @endif

  <div class="col-sm-12 discover-experience-main">
    <div class="product-details"><h2>{{ucfirst(__('all.similar products'))}}</h2></div>

    @include('front.components.products_grid',['products' => $similar_products])

  </div><!-- col-sm-12  discover-experience-main -->

</div><!-- container -->
</div>
@endsection

@include('front.components.product_config_modal')

@push('footer_scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />

<style media="screen">
td.day.active{
  background: #c16147 !important;
}
</style>
<script type="text/javascript">

var product = @json($product);
var availabilities = product.availabilities;
var free_transfer = product.free_transfer;
var price_rules = product.pricerules;
var free_transfer_up_to = product.free_transfer_up_to;
var enabledDates = @json(array_values($product->availabilities->pluck('day')->unique()->toArray()));
var selectedDate;
var selectedTime;
var availableTimes;
var total = 0;

var currencyFormatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'EUR',
});

var countChanges = 0;
$('.checkbox-addon').change(function(){
  //$(this).find('label').hasClass('active');
  countChanges++;
  if(countChanges == 3){
      calculateTotals();
      countChanges = 0;
  }
})

function calculateTotals(){

  total = 0;
  var children = parseInt($('#children_num').val());
  var adults = parseInt($('#adults_num').val());
  var participants = children+adults;
  var transferred_soil = Math.ceil(participants/free_transfer_up_to);
  var free_transfer_in_math = free_transfer*transferred_soil;

  var adultsTransferWeight = adults/participants*free_transfer_in_math;
  var childrenTransferWeight = children/participants*free_transfer_in_math;
  var price_per_adult = 0;
  var price_per_child = 0;
  var adults_price = 0;
  var children_price = 0;
  var discount = 0;
  selectedTime = $('#available_times').val();
  //console.log(selectedTime);
  for(var i in availabilities){
    var av = availabilities[i];
    //console.log('comoparing '+moment(av.day).format('L')+' with '+moment(selectedDate).format('L')+' and '+moment(selectedTime,'HH:mm:ss').format('HH:mm')+ ' with '+moment(av.time,'HH:mm:ss').format('HH:mm'));
    if(moment(av.day).format('L') == moment(selectedDate).format('L') && moment(selectedTime,'HH:mm:ss').format('HH:mm') == moment(av.time,'HH:mm:ss').format('HH:mm')){
      price_per_adult = parseFloat(av.price_per_adult);
      price_per_child = parseFloat(av.price_per_child);

      //edit price per adult and child due to price rules

        if(typeof(price_rules) != 'undefined'){
            for(var j in price_rules){
                var rule = price_rules[j];
                if(participants >= rule.from_participants && participants <= rule.to_participants){
                    discount = rule.discount;
                    price_per_adult*= 1 - rule.discount/100;
                    price_per_child*= 1 - rule.discount/100;
                }
            }
        }


        //end price rules

      adults_price = price_per_adult * adults;
      children_price = price_per_child * children;
      total += adults_price + children_price + free_transfer_in_math;
      var price_per_adult_to_show = price_per_adult+(adultsTransferWeight/adults);
      var price_per_child_to_show = price_per_child+(childrenTransferWeight/children);
      if(isNaN(price_per_child_to_show)){
        price_per_child_to_show = 0.00;
      }

      var addOnTotal = 0;
      var addons = [];

      $('.checkbox-addon').each(function(){
        if($(this).is(':checked')){
          //console.log('price per adults '+parseFloat($(this).data('price_per_adult'))+' price per child '+parseFloat($(this).data('price_per_child')));
          var addonAdults = parseFloat($(this).data('price_per_adult'))*adults;
          var addonChildren = parseFloat($(this).data('price_per_child'))*children;
          addOnTotal+= addonAdults + addonChildren;
          addons.push($(this).data('id'));
          var selectorAdults = '#tot_add_on_'+$(this).data('id')+'_adults';
          var selectorChildren = '#tot_add_on_'+$(this).data('id')+'_children';


          $(selectorAdults).html(currencyFormatter.format(addonAdults));
          $(selectorChildren).html(currencyFormatter.format(addonChildren));
        }else{
          var selectorAdults = '#tot_add_on_'+$(this).data('id')+'_adults';
          var selectorChildren = '#tot_add_on_'+$(this).data('id')+'_children';
          $(selectorAdults).html('€0,00');
          $(selectorChildren).html('€0,00');
        }
      })

      $('#input_addons').val(addons.join(','));

      //console.log(addOnTotal);

      total+=addOnTotal;

      $('#price_per_adult_show').html('('+currencyFormatter.format(price_per_adult_to_show)+' per adult)');
      $('#price_per_child_show').html('('+currencyFormatter.format(price_per_child_to_show)+' per child)');
      $('.grand-total').html(currencyFormatter.format(total));
      $('#adults_price_summary').html(currencyFormatter.format(adults_price+adultsTransferWeight)+' ('+currencyFormatter.format(price_per_adult_to_show)+' x '+adults+' adults)');
      $('#children_price_summary').html(currencyFormatter.format(children_price+childrenTransferWeight)+' ('+currencyFormatter.format(price_per_child_to_show)+' x '+children+' children)');
    }
  }
  $('#input_adults').val(adults);
  $('#input_children').val(children);
  $('#input_date').val(moment(selectedDate).format('L'));
  $('#input_time').val(moment(selectedTime,'HH:mm:ss').format('HH:mm'));
  console.log({
    adults:adults,
    children:children,
    adults_price:adults_price,
    children_price:children_price,
    transferred_soil:transferred_soil,
    free_transfer_in_math:free_transfer_in_math,
    total:total,
    adultsTransferWeight:adultsTransferWeight,
    childrenTransferWeight:childrenTransferWeight,
    price_per_adult_to_show:price_per_adult_to_show,
    price_per_child_to_show:price_per_child_to_show,
      discount:discount
  });
}

function updateAvailableTimes(){

  console.log('Begin update available times');
  console.log('selected date '+selectedDate);
  availableTimes = [];
  $('#available_times').html('');
  $('td.day.today').removeClass('today');//shitty method

  for(var i in availabilities){
    var av = availabilities[i];
    if(moment(av.day).format('L') == moment(selectedDate).format('L')){
      availableTimes.push(av.time);
    }
  }
  for(var i in availableTimes){
    $('#available_times').append('<option value="'+availableTimes[i]+'">'+availableTimes[i].substr(0,5)+'</option>')
  }
  calculateTotals();
  console.log(availableTimes);

}

$(function(){
  selectedDate = enabledDates[0]
  availableTimes = [];
  $('#calendar_first').datetimepicker({
    inline: true,
    sideBySide: true,
    format: 'L',
    enabledDates:enabledDates,
    defaultDate:enabledDates[0],
  });
  $('#calendar_first').on("change.datetimepicker",function(e){
    selectedDate = e.date;
    updateAvailableTimes();
  })

  $('td.day.today').removeClass('today');//shitty method

  updateAvailableTimes();
})

// ***dob and checkin close*** //
// ********qunantity minus plus start*********** //
//plugin bootstrap minus and plus
//http://jsfiddle.net/laelitenetwork/puJ6G/
$('.btn-number').click(function(e){

  e.preventDefault();

  fieldName = $(this).attr('data-field');
  type      = $(this).attr('data-type');
  var input = $("input[name='"+fieldName+"']");
  var currentVal = parseInt(input.val());
  if (!isNaN(currentVal)) {
    if(type == 'minus') {

      if(currentVal > input.attr('min')) {
        input.val(currentVal - 1).change();
      }
      if(parseInt(input.val()) == input.attr('min')) {
        $(this).attr('disabled', true);
      }

    } else if(type == 'plus') {

      if(currentVal < input.attr('max')) {
        input.val(currentVal + 1).change();
      }
      if(parseInt(input.val()) == input.attr('max')) {
        $(this).attr('disabled', true);
      }

    }
  } else {
    input.val(0);
  }

  calculateTotals();

});
$('.input-number').focusin(function(){
  $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {

  minValue =  parseInt($(this).attr('min'));
  maxValue =  parseInt($(this).attr('max'));
  valueCurrent = parseInt($(this).val());

  name = $(this).attr('name');
  if(valueCurrent >= minValue) {
    $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
  } else {
    alert('Sorry, the minimum value was reached');
    $(this).val($(this).data('oldValue'));
  }
  if(valueCurrent <= maxValue) {
    $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
  } else {
    alert('Sorry, the maximum value was reached');
    $(this).val($(this).data('oldValue'));
  }


});
$(".input-number").keydown(function (e) {
  // Allow: backspace, delete, tab, escape, enter and .
  if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
  // Allow: Ctrl+A
  (e.keyCode == 65 && e.ctrlKey === true) ||
  // Allow: home, end, left, right
  (e.keyCode >= 35 && e.keyCode <= 39)) {
    // let it happen, don't do anything
    return;
  }
  // Ensure that it is a number and stop the keypress
  if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    e.preventDefault();
  }
});
// ********qunantity minus plus close*********** //
</script>
@endpush
