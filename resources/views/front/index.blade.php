<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Talixo Tuscany</title>
  <!-- Custom fonts for this theme -->
  <link href="{{asset('front/css/fontawesome.min.css')}}" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Theme CSS -->
  <link href="{{asset('front/css/style.css').'?v='.rand(0,10000)}}" rel="stylesheet" type="text/css">
  <link href="{{asset('front/css/bootstrap.css')}}" rel="stylesheet"  type="text/css">
  <link href="{{asset('front/css/bootstrap-datepicker.css')}}" rel="stylesheet" type="text/css">
  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('front/js/jquery.js')}}"></script>
  <script src="{{asset('front/js/bootstrap.js')}}" ></script>
  <script src="{{asset('front/js/bootstrap.min.js')}}"></script>
  <!--<script src="{{asset('front/js/jquery.min.js')}}"></script>-->
  <script src="{{asset('front/js/bootstrap.bundle.min.js')}}"></script>
  <!-- Plugin JavaScript -->
  <script src="{{asset('front/js/jquery.easing.min.js')}}"></script>
  <!-- Contact Form JavaScript -->
  <script src="{{asset('front/js/bootstrap-validation.js')}}"></script>

  <script src="{{asset('front/js/bootstrap-datepicker.js')}}"></script>
  <script src="{{asset('front/js/moment-with-locales.js')}}"></script>

  <script src="{{asset('front/js/scripts.js')}}"></script>

  <!--<meta name="google" content="notranslate" />-->
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-159556839-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-159556839-1');
</script>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '563067444637840');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=563067444637840&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="4daebedc-8e43-4e47-99f6-a26249f2bb81" data-blockingmode="auto" type="text/javascript"></script>
</head>

<body class="home-page">
  @yield('header_jumbotron')
  @yield('page_main')
  @yield('adv_banner')
  @yield('pre_footer')
  @include('front.components.footer')
  @stack('footer_scripts')
</body>
</html>
