@extends('front.index');

@section('header_jumbotron')
<div class="common-page">
  <div class="jumbotron banner-head">
    <div class="container">

      @include('front/components/common_page_header')

    </div>
  </div> <!-- banner-head -->
</div>
@endsection

@section('page_main')

  <div class="container">
    <div class="row my-4 py-4">
      <div class="col-md-12 text-center">
        <h1>Thank you!</h1>
        <p>
          Thank your for your purchase!
        </p>
      </div>
    </div>
  </div>

@endsection
