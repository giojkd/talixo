@extends('front.index')

@section('header_jumbotron')
<div class="common-page">
  <div class="jumbotron banner-head">
    <div class="container">

      @include('front/components/common_page_header')

    </div>
  </div> <!-- banner-head -->
</div>
@endsection

@section('page_main')
<div class="container-fluid pt-4">

  <div class="container">
    <div class="row">
      <div class="col">
        <h1 class="mb-4">The complete selection of our marvelous experiences</h1>
        @foreach($productGroups as $products)
        <h1>{{$products[0]->category->name}}</h1>
        <div class="row">
          @include('front.components.products_grid')
        </div>
        @endforeach

      </div>
    </div>
  </div>



</div>
@endsection
