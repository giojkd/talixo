@extends('front.index')

@section('header_jumbotron')
    <div class="common-page">
        <div class="jumbotron banner-head">
            <div class="container">

                @include('front/components/common_page_header')

            </div>
        </div> <!-- banner-head -->
    </div>
@endsection

@section('page_main')
    <div class="container-fluid pt-4">

        <div class="container">
            <div class="row">

                <div class="col-sm-6 left-main">
                    <form id="pay-form" method="post" action="{{route('voucherCheckout')}}">
                        @csrf
                        <h2>{{ucfirst(__('all.buy a voucher'))}}!</h2>
                        <p>
                            {!!ucfirst(__('all.buy a voucher intro'))!!}
                        </p>

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                        <div class="checkout-second-step">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>1 - {{ucfirst(__('all.choose an amount'))}}</h3>
                                    <p>{{ucfirst(__('all.any value will give you at least and extra 10%'))}}!</p>
                                </div>
                                <div class="form-group  col-sm-12 mb-4">
                                    <select class="form-control" id="" name="grand_total">

                                        @for($i = 1; $i < 100 ; $i++)
                                            <option value="{{$i*100}}">{{ucfirst(__('all.costs and gives',['cost' => $i*100,'gives' => $i*110]))}}€
                                            </option>
                                        @endfor

                                    </select>
                                </div>

                            </div>
                        </div>

                        <!--<div class="caution col-sm-12"></div>-->


                        <div class="checkout-second-step">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>2 - {{ucfirst(__('all.billing address'))}} </h3>
                                    <p>{{ucfirst(__('all.all the fields are required'))}}</p></div>
                                <div class="form-group  col-sm-6"><input value="{{old('client_name')}}" required
                                                                         name="client_name" type="text"
                                                                         class="form-control"
                                                                         placeholder="{{ucfirst(__('all.name'))}}">
                                </div>
                                <div class="form-group  col-sm-6"><input value="{{old('client_surname')}}" required
                                                                         name="client_surname" type="text"
                                                                         class="form-control"
                                                                         placeholder="{{ucfirst(__('all.surname'))}}">
                                </div>

                                <div class="form-group  col-sm-6"><input value="{{old('client_email')}}" required
                                                                         name="client_email" type="text"
                                                                         class="form-control" placeholder="Email"></div>
                                <div class="form-group  col-sm-6"><input value="{{old('client_telephone')}}" required
                                                                         name="client_telephone" type="text"
                                                                         class="form-control"
                                                                         placeholder="{{ucfirst(__('all.telephone'))}}">
                                </div>

                                <div class="form-group  col-sm-12"><input id="billing_address"
                                                                          value="{{old('client_address')}}" required
                                                                          name="client_address" type="text"
                                                                          class="form-control"
                                                                          placeholder="{{ucfirst(__('all.address'))}}">
                                </div>

                                <div class="form-group  col-sm-12"><textarea value="{{old('note')}}" name="notes"
                                                                             placeholder="{{ucfirst(__('all.allergies, diary restrictions or any other request'))}}"></textarea>
                                </div>
                            </div>
                        </div><!-- checkout-second-step -->


                        <div class="checkout-third-step">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>3 - {{ucfirst(__('all.payment'))}}</h3>
                                    <p></p>

                                    <div class="radio-payment">
                                        <div class="radio credit-payment">
                                            <div class="radio-left col-sm-2">
                                                <input name="payment_method" value="credit_card" type="radio" checked>
                                            </div>
                                            <div class="radio-right  col-sm-10">
                                                <p class="pl-0">{{ucfirst(__('all.pay with credit card'))}} <img
                                                        src="{{asset('front/images/visa.png')}}"></p>
                                                <div class="form-group  col-sm-12"><input name="card[num]" type="text"
                                                                                          class="form-control"
                                                                                          value="{{old('card.num')}}"
                                                                                          placeholder="{{ucfirst(__('all.credit card number'))}}">
                                                </div>
                                                <div class="form-group  col-sm-12"><input name="card[name]" type="text"
                                                                                          class="form-control"
                                                                                          value="{{old('card.name')}}"
                                                                                          placeholder="{{ucfirst(__('all.cardholder name'))}}">
                                                </div>
                                                <div class="form-group  col-sm-12">
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <input name="card[mm]" type="text" class="form-control"
                                                                   value="{{old('card.mm')}}"
                                                                   placeholder="{{ucfirst(__('all.exp'))}}. mm">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <input name="card[yy]" type="text" class="form-control"
                                                                   value="{{old('card.yy')}}"
                                                                   placeholder="{{ucfirst(__('all.exp'))}}. yy">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <input name="card[ccv]" type="text" class="form-control"
                                                                   value="{{old('card.ccv')}}"
                                                                   placeholder="CCV">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- radio -->

                                        <!--
                                        <div class="radio">
                                        <div class="radio-left col-sm-2">
                                        <input name="payment_method" value="paypal" type="radio">
                                      </div>
                                      <div class="radio-right  col-sm-10">
                                      <p>Pay with con PayPal <img src="images/paypal.png"></p>
                                    </div>
                                  </div>

                                  <div class="radio">
                                  <div class="radio-left  col-sm-2">
                                  <input name="payment_method" value="bank_wire_transfer" type="radio">
                                </div>
                                <div class="radio-right  col-sm-10">
                                <p>Pay with bank wire transfer <img src="images/bonifico.png"></p>
                              </div>
                            </div>

                          -->

                                    </div>
                                </div><!-- radio-payment-->

                            </div>
                        </div><!-- checkout-second-step -->
                        <button type="submit" class="btn-talixo-light d-inline-block">{{ucfirst(__('all.confirm and buy'))}}!</button>
                    </form>
                </div><!-- col-sm-4 left-main -->

                <div class="col-sm-6 voucher-grants text-center">

                    <div class="voucher-grant">
                        {!!__('all.voucher grant 1')!!}
                    </div>

                    <div class="voucher-grant">
                        {!!__('all.voucher grant 2')!!}
                    </div>

                    <div class="voucher-grant">
                        {!!__('all.voucher grant 3')!!}
                    </div>

                    <div class="voucher-grant">
                        {!!__('all.voucher grant 4')!!}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
