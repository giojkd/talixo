@extends('front.index')

@section('header_jumbotron')
<div class="common-page">
  <div class="jumbotron banner-head">
    <div class="container">

      @include('front/components/common_page_header')

    </div>
  </div> <!-- banner-head -->
</div>
@endsection

@section('page_main')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-5 px-0">
      <div id="map" class="stick-to-top vh-100 w-100">

      </div>
    </div>
    <div class="col-md-7 map-grid text-right">
      @foreach($productGroups as $products)
      <h1 id="geofence-{{$products[0]->geofence->slug}}">{{$products[0]->geofence->name}}</h1>
      <div class="row">


        @foreach($products as $product)
        @if(isset($product->getPhotos()[0]))
        <a href="{{$product->getUrl('product')}}" class="col-sm-4 product-grid-element">
          <div class="discover-inner">
            <div class="discover-img-wrapper">


              <div class="discover-img" style="background:url('{{url($product->getCover())}}'); background-size: cover; background-position: center center;">


              </div><!-- discover-img -->
              <span class="sell-out">LIKELY TO SELL OUT</span>
              <!--<span class="like-heart"><img src="http://localhost:8000/front/images/heart-icon.png"></span>-->
              <!--<div class="blue-area"> <span class="rate">8,8</span> <span class="avg">su 1050</span></div>-->
            </div>

            <div class="discover-data">
              <h3 class="location-name">{{Str::limit($product->name,28,$end='')}}</h3>
              <p>{{Str::limit($product->short_description, 50, $end='...')}}</p>

              @if($product->availabilities->count() > 0)
              <p>From <span class="travel-price">{{$product->availabilities->min('price_per_adult')}} <small>EUR</small></span> per person</p>
              @endif

              <div class="map-duration">
                <span class="map-loc">{{$product->geofence->name}}</span>
                <span class="jounery-duration">{{Str::limit($product['duration'],5,$end='')}} hours</span>
                <!--<span class="star-rating">8,8(su1050)</span>-->
              </div><!-- map-duration -->

              @if($product->availabilities->count() > 0)
              <div class="calendar-checkin-checkout">
                @foreach($product->availabilities as $index => $availability)
                @if($index < 2)
                <span>{{$availability->day->format('D d')}} {{Str::limit($availability['time'],5,$end='')}}</span>
                @endif
                @endforeach

                @if(($product->availabilities->count()-2) > 0)
                <br>
                <span class="more-option">+ {{$product->availabilities->count()-2}} more</span>
                @endif
              </div>
              <div class="clearfix">

              </div>
              @endif
            </div><!-- discover-data -->
          </div>
        </a><!-- discover-inner -->
        @endif
        @endforeach

      </div>
      @endforeach
    </div>

  </div>
</div>
@endsection

@push('footer_scripts')

<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API_KEY')}}"></script>
<script src="{{asset('front/js/gmaps-marker-label.js')}}"></script>

<script type="text/javascript">

var labelMarkers = [];
var coordinates = @json($coordinates);



function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 8.5,
    center: {lat: 43.41463, lng: 11.12824}, //currently colle val d'elsa
    mapTypeId: 'terrain',
    styles: mapStyle,
    mapTypeId: 'roadmap',
    disableDefaultUI: true,
    zoomControl: false,
    minZoom:8.5,
    maxZoom:8.5
  });

  // Define the LatLng coordinates for the polygon's path.

  for(var i in coordinates){
    console.log(i);
    var polyCoords = [];
    var polygon = coordinates[i];


    labelMarkers.push(new MarkerWithLabel({
      position: {lat: parseFloat(polygon[0][1]), lng: parseFloat(polygon[0][0])},
      map: map,
      icon: '/front/images/pixtras.png',
      labelContent: i,
      labelAnchor: new google.maps.Point(20, 20),
      // the CSS class for the label
      labelClass: "mapLabel",
      labelInBackground: true
    }));


    console.log(polygon);

    for (var j in polygon){
      var point = polygon[j];
      polyCoords.push({lat:parseFloat(point[1]),lng:parseFloat(point[0])})
    }
    var poly = new google.maps.Polygon({
      paths: polyCoords,
      geodesic: true,
      strokeColor: '#c16147',
      strokeOpacity: 0,
      strokeWeight: 4,
      fillColor: '#c16147',
      fillOpacity: 0.55,
      title: 'cane',
      geofence: i
    });
    google.maps.event.addListener(poly, 'click', function (event) {
      scrollToDiv("#geofence-"+this.geofence)
    });
    poly.setMap(map);
  }


  // Construct the polygon.


}

google.maps.event.addDomListener(window, 'load', initMap);
</script>



<!--
<script src="https://maps.googleapis.com/maps/api/staticmap?key={{env('GOOGLE_API_KEY')}}&callback=initMap&center=43.14866881320332,10.344540529188547&zoom=8&format=png&maptype=roadmap&style=element:geometry%7Ccolor:0xf5f5f5&style=element:labels%7Cvisibility:off&style=element:labels.icon%7Cvisibility:off&style=element:labels.text.fill%7Ccolor:0x616161&style=element:labels.text.stroke%7Ccolor:0xf5f5f5&style=feature:administrative%7Celement:geometry%7Cvisibility:off&style=feature:administrative.land_parcel%7Cvisibility:off&style=feature:administrative.land_parcel%7Celement:labels.text.fill%7Ccolor:0xbdbdbd&style=feature:administrative.neighborhood%7Cvisibility:off&style=feature:administrative.province%7Ccolor:0xc16147%7Cvisibility:on%7Cweight:8&style=feature:poi%7Cvisibility:off&style=feature:poi%7Celement:geometry%7Ccolor:0xeeeeee&style=feature:poi%7Celement:labels.text.fill%7Ccolor:0x757575&style=feature:poi.park%7Celement:geometry%7Ccolor:0xe5e5e5&style=feature:poi.park%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&style=feature:road%7Celement:geometry%7Ccolor:0xffffff&style=feature:road%7Celement:labels.icon%7Cvisibility:off&style=feature:road.arterial%7Cvisibility:off&style=feature:road.arterial%7Celement:labels.text.fill%7Ccolor:0x757575&style=feature:road.highway%7Celement:geometry%7Ccolor:0xdadada&style=feature:road.highway%7Celement:labels%7Cvisibility:off&style=feature:road.highway%7Celement:labels.text.fill%7Ccolor:0x616161&style=feature:road.local%7Cvisibility:off&style=feature:road.local%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&style=feature:transit%7Cvisibility:off&style=feature:transit.line%7Celement:geometry%7Ccolor:0xe5e5e5&style=feature:transit.station%7Celement:geometry%7Ccolor:0xeeeeee&style=feature:water%7Celement:geometry%7Ccolor:0xc9c9c9&style=feature:water%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&size=480x360" async defer></script>
-->

<!--

https://maps.googleapis.com/maps/api/staticmap?key=YOUR_API_KEY&center=43.59399170685145,10.448056743460352&zoom=9&format=png&maptype=roadmap&style=element:geometry%7Ccolor:0xf5f5f5&style=element:labels%7Cvisibility:off&style=element:labels.icon%7Cvisibility:off&style=element:labels.text.fill%7Ccolor:0x616161&style=element:labels.text.stroke%7Ccolor:0xf5f5f5&style=feature:administrative%7Celement:geometry%7Cvisibility:off&style=feature:administrative.country%7Celement:geometry.stroke%7Ccolor:0xc16147%7Cvisibility:on%7Cweight:2&style=feature:administrative.land_parcel%7Cvisibility:off&style=feature:administrative.province%7Celement:geometry.stroke%7Ccolor:0xc16147%7Cvisibility:on%7Cweight:2.5&style=feature:poi%7Cvisibility:off&style=feature:poi%7Celement:geometry%7Ccolor:0xeeeeee&style=feature:poi%7Celement:labels.text.fill%7Ccolor:0x757575&style=feature:poi.park%7Celement:geometry%7Ccolor:0xe5e5e5&style=feature:poi.park%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&style=feature:road%7Cvisibility:off&style=feature:road%7Celement:geometry%7Ccolor:0xffffff&style=feature:road%7Celement:labels.icon%7Cvisibility:off&style=feature:road.arterial%7Celement:labels.text.fill%7Ccolor:0x757575&style=feature:road.highway%7Celement:geometry%7Ccolor:0xdadada&style=feature:road.highway%7Celement:labels.text.fill%7Ccolor:0x616161&style=feature:road.local%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&style=feature:transit%7Cvisibility:off&style=feature:transit.line%7Celement:geometry%7Ccolor:0xe5e5e5&style=feature:transit.station%7Celement:geometry%7Ccolor:0xeeeeee&style=feature:water%7Celement:geometry%7Ccolor:0xc9c9c9&style=feature:water%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&size=480x360

-->


@endpush
