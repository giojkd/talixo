@extends('front.index')

@section('header_jumbotron')

<style media="screen">
@-webkit-keyframes zoom {
  from {
    -webkit-transform: scale(1, 1);
  }
  to {
    -webkit-transform: scale(1.5, 1.5);
  }
}

@keyframes zoom {
  from {
    transform: scale(1, 1);
  }
  to {
    transform: scale(1.5, 1.5);
  }
}

.carousel-inner .carousel-item {
  -webkit-animation: zoom 20s;
  animation: zoom 20s;
}
</style>
<style media="screen">
.slideshowHomepage{

}
.slideshowHomepage .carousel-inner .carousel-item{
  height: 750px;
  background-position: center center;
  background-repeat: no-repeat;
  background-size: cover;
}
</style>

<div class="position-relative" style=" height: 900px;">

  <div id="carouselExampleControls" class="d-block d-md-none slideshowHomepage carousel carousel-fade slide d-block position-absolute w-100" data-interval="7000" data-ride="carousel">
    <div class="carousel-inner w-100">


      @foreach(array_values(json_decode($slideshows['homepage_main_slider']->files,1)) as $index => $photo)
      <div class="carousel-item @if($index == 0) active @endif w-100" style="background-image: url({{url($photo)}})"></div>

      @endforeach
    </div>

  </div>

<video width="100%" autoplay="autoplay" loop="true" muted class="d-none d-md-block">
  <source src="{{ url('/front/videos/background-video.mp4') }}" type="video/mp4">

Your browser does not support the video tag.
</video>
  <div class="jumbotron banner-head" style="z-index: 999; position: absolute; top: 0px; left: 0px; width: 100%; background: transparent;">
    <div class="container">
      <header>
        <div class="logo">
          <a href="{{route('home')}}">
            <img src="{{asset('front/images/talixo-logo.png')}}">
          </a>
        </div>
        <!--
        <div class="search-bar">
        <div class="form-group search-web">
        <input type="text" class="form-control" placeholder="Cerca">
      </div>
    </div>-->
    <div class="menu-bar">
      <div class="container-fluid">
        <nav class="navbar navbar-expand-sm  navbar-dark">
          <!-- <a class="navbar-brand" href="#">Navbar</a> -->
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="{{route('vouchers')}}" class="nav-link special-link">
                        <i class="fa fa-gift"></i> {{ucfirst(__('all.gift'))}}
                    </a>
                </li>
              @foreach($menus['top_menu']->articles as $article)
              <li class="nav-item">
                <a class="nav-link" href="{{$article->getUrl('article')}}">{{$article->name}}</a>
              </li>
              @endforeach
              <li class="nav-item">
                <a class="nav-link" href="{{route('contactUs')}}">{{ucfirst(__('all.contact us'))}}</a>
              </li>
              <li class="nav-item">
                <a href="{{route('experiences')}}" class="nav-link special-link">
                  <i class="fa fa-star"></i> {{ucfirst(__('all.experiences'))}}
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('map')}}" class="nav-link special-link">
                  <i class="fa fa-glass"></i> {{ucfirst(__('all.beyond the label'))}}
                </a>
              </li>


              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {{substr($languages[app()->getLocale()]->name,0,3)}}
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  @foreach($languages as $index => $language)
                    @if($index != app()->getLocale())
                      <a href="{{route('languageSwitcher',['language' => $language->iso])}}" class="dropdown-item" >{{substr($language->name,0,3)}}</a>
                    @endif
                  @endforeach
                </div>
              </li>
              <!--
              <li class="country-symbol"> <span class="currency">€</span> <span class="flag"><img src="/front/images/country.jpg"></span></li>
              <li class="login-registration"> <a href="#">Accedi</a> <a href="#">Registrati</a></li>-->
            </ul>
          </div>
        </nav>
      </div>
    </div>
  </header>

  <div class="explore-amazing">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-5">
          <h2>{{__('all.Explore our amazing selection of unique experiences')}}</h2>
          <div class="search-widget-categories">
            @foreach($categories as $category)
            <a href="{{$category->getUrl('category')}}">{{$category->name}}</a>
            @endforeach
          </div>
          <!--<div class="form-group search-place"> <input type="text" class="form-control" placeholder="Where are you going ?"> </div>

          <div class="form-group calendar-new">
          <div class='input-group date' id='checkin'>
          <input type='text' class="form-control"  placeholder="Check in - Check out " />
          <span class="input-group-addon">
          <span class="glyphicon glyphicon-calendar"></span>
        </span>
      </div>
    </div>

    <div class="form-group people-place"> <input type="text" class="form-control" placeholder="2 Adulti  -  0 Bambini"> </div>

    <div class="search-place"> <input type="submit" value="Cerca"> </div>-->
  </div>
</div>
</div>
</div><!-- explore-amazing -->

</div>
</div> <!-- banner-head -->
</div>
@endsection

@section('page_main')
<div class="container">

  <div class="col-sm-12 company-features">
    <div class="row">
      <div class="col-sm-3 company-features-inner">
        <img src="/front/images/year-icon.png">
        <div class="company-inner-data">
          <h3 data-slug="homepage_box_1_slug">{{$textBlocks['homepage_box_1_slug']}}</h3>
          <p data-slug="homepage_box_1_content">{{$textBlocks['homepage_box_1_content']}}</p>
        </div><!-- company-inner-data -->
      </div>
      <div class="col-sm-3 company-features-inner">
        <img src="/front/images/partner-icon.png">
        <div class="company-inner-data">
          <h3 data-slug="homepage_box_2_slug">{{$textBlocks['homepage_box_2_slug']}}</h3>
          <p data-slug="homepage_box_2_content">{{$textBlocks['homepage_box_2_content']}}</p>
        </div><!-- company-inner-data -->
      </div>
      <div class="col-sm-3 company-features-inner">
        <img src="/front/images/exp-icon.png">
        <div class="company-inner-data">
          <h3 data-slug="homepage_box_3_slug">{{$textBlocks['homepage_box_3_slug']}}</h3>
          <p data-slug="homepage_box_3_content">{{$textBlocks['homepage_box_3_content']}}</p>
        </div><!-- company-inner-data -->
      </div>
      <div class="col-sm-3 company-features-inner">
        <img src="/front/images/customize_icon.png">
        <div class="company-inner-data">
          <h3 data-slug="homepage_box_4_slug">{{$textBlocks['homepage_box_4_slug']}}</h3>
          <p data-slug="homepage_box_4_content">{{$textBlocks['homepage_box_4_content']}}</p>
        </div><!-- company-inner-data -->
      </div>
    </div>
  </div><!-- col-sm-12  company-features -->


  <div class="col-sm-12 discover-experience-head">
    <div class="row">
      <div class="col-sm-12 discover-experience-inner">
        <h2 data-slug="homepage_caption_1_title">{{$textBlocks['homepage_caption_1_title']}}</h2>
        <p data-slug="homepage_caption_1_content">{{$textBlocks['homepage_caption_1_content']}}</p>
      </div>
    </div>
  </div><!-- discover-experience-head -->

  @include('front.components.products_grid')

    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12 text-center">
                <a href="{{route('experiences')}}" class="btn-talixo-light d-inline-block" style="width: auto!important;">Show more opportunities</a>
            </div>
        </div>
    </div>


  <!-- col-sm-12  discover-experience-main -->

</div><!-- container -->
@endsection

@section('adv_banner')
<!--
<div class="custom-travel-main">
<div class="container">
<div class="col-sm-12">
<div class="row">
<div class="col-sm-4">
<div class="red-area">
<p>CUSTOMIZE</p>
<p class="your-half">YOUR</p>
<p>TRAVEL</p>
</div>
<a class="grey-btn" href="#">Design your dream travel <span> &gt; </span></a>
</div>
</div>
</div>
</div>
</div>-->
@endsection


@section('pre_footer')

<div style="height: 100px; width: 100%;"></div>

<div class="container" >
  <div class="col-sm-12 discover-experience-head inspiration-head">
    <div class="row">
      <div class="col-sm-12 discover-experience-inner">
        <h2 data-slug="homepage_caption_2_title">{{$textBlocks['homepage_caption_2_title']}}</h2>
        <p data-slug="homepage_caption_2_content">{{$textBlocks['homepage_caption_2_content']}}</p>
      </div>
    </div>
  </div><!-- discover-experience-head -->

  <div class="col-sm-12 inspiration-travel">
    <div class="row">
      @foreach($categories as $index => $category)
      @if($index < 6)
      <div class="col-sm-4">

        <a href="{{$category->getUrl('category')}}" class="inspiration-travel-inner" style="background:url('/{{$category->cover}}') no-repeat center center;">
          <div class="inspiration-travel-inner-banner">
            <span>{{$category->name}}</span>
          </div>
          <p>{{$category->products_count}} {{__('all.experiences')}}</p>
        </div>

      </a>
      @endif
      @endforeach
      <!--
      <div class="col-sm-4 inspiration-travel-inner">
      <img src="/front/images/dine_me.png">
      <p>93 experiences</p>
    </div>

    <div class="col-sm-4 inspiration-travel-inner">
    <img src="/front/images/museums.png">
    <p>53 experiences</p>
  </div>

  <div class="col-sm-4 inspiration-travel-inner">
  <img src="/front/images/dine_me_2.png">
  <p>93 experiences</p>
</div>

<div class="col-sm-4 inspiration-travel-inner cross1">
<img src="/front/images/museums_2.png">
<p>53 experiences</p>
</div>

<div class="col-sm-4 inspiration-travel-inner cross2">
<img src="/front/images/bike_tour_2.png">
<p>23 experiences</p>
</div>-->
</div>
</div><!-- inspiration-travel -->


@if(($categories->count()-6) > 0)
<div class="col-sm-12 suggestion-tag">
  <div class="row">
    @foreach($categories->splice(6)->chunk(3) as $index =>  $categories_chunk)
    <ul>
      @foreach($categories_chunk as $category)
      <li> <a href="{{$category->getUrl('category')}}"> {{$category->name}} </a> </li>
      @endforeach
    </ul>
    @endforeach
    <!--<p class="grey-btn-area-para"><a class="grey-btn" href="#">Discover over 50 suggestioons <span> &gt; </span></a></p>-->
  </div>
</div><!-- suggestion-tag -->
@endif
</div><!-- container -->

@endsection

@push('footer_scripts')

@endpush
