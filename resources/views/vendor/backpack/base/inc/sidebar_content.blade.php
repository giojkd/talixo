<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->


<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('vehicle') }}'><i class='nav-icon fa fa-car'></i> Vehicles</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('specialpoint') }}'><i class='nav-icon fa fa-map-marker'></i> Special points</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('specialpointpriceoption') }}'><i class='nav-icon fa fa-car'></i><i class='nav-icon fa fa-map-marker'></i> Special point price options</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('transferbooking') }}'><i class='nav-icon fa fa-book'></i> Transfer bookings</a></li>
<li class='nav-item'><hr></li>
<li class=nav-item><a class=nav-link href="{{ backpack_url('elfinder') }}"><i class="nav-icon fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('category') }}'><i class='nav-icon fa fa-list'></i> Categories</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('product') }}'><i class='nav-icon fa fa-list'></i> Products</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('availability') }}'><i class='nav-icon fa fa-calendar'></i> Availabilities</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('geofence') }}'><i class='nav-icon fa fa-map-marker'></i> Geofences</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('review') }}'><i class='nav-icon fa fa-star'></i> Reviews</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('language') }}'><i class='nav-icon fa fa-flag'></i> Languages</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('article') }}'><i class='nav-icon fa fa-font'></i> Articles</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('lead') }}'><i class='nav-icon fa fa-question'></i> Leads</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('leadrow') }}'><i class='nav-icon fa fa-question'></i> Leadrows</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('menu') }}'><i class='nav-icon fa fa-question'></i> Menus</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('textblock') }}'><i class='nav-icon fa fa-question'></i> Textblocks</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('slideshow') }}'><i class='nav-icon fa fa-question'></i> Slideshows</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('productaddon') }}'><i class='nav-icon fa fa-question'></i> Productaddons</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('pricerule') }}'><i class='nav-icon fa fa-question'></i> Pricerules</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('voucher') }}'><i class='nav-icon fa fa-question'></i> Vouchers</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('event') }}'><i class='nav-icon fa fa-question'></i> Events</a></li>

<li class='nav-item'><a class='nav-link' href='{{ route('availabilityBulkGet') }}'><i class='nav-icon fa fa-question'></i> Availability Bulk</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('specialpoint') }}'><i class='nav-icon fa fa-question'></i> Specialpoints</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('specialpointpriceoption') }}'><i class='nav-icon fa fa-question'></i> Specialpointpriceoptions</a></li>

