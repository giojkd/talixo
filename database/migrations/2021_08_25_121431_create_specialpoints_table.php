<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecialpointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specialpoints', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->float('lat',10,6)->nullable();
            $table->float('lng',10,6)->nullable();
            $table->json('point')->nullable();
            $table->string('type')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specialpoints');
    }
}
