<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableProductaddons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productaddons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->json('name')->nullable();
            $table->json('description')->nullable();
            $table->string('cover')->nullable();
            $table->float('price_per_adult')->nullable();
            $table->float('price_per_child')->nullable();
            $table->integer('product_id')->index()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productaddons');
    }
}
