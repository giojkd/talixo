<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('client_name')->nullable();
            $table->string('client_surname')->nullable();
            $table->string('client_email')->nullable();
            $table->string('client_telephone')->nullable();
            $table->string('client_address')->nullable();
            $table->text('notes')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('pickup_address')->nullable();
            $table->double('pickup_address_latitude')->nullable();
            $table->double('pickup_address_longitude')->nullable();
            $table->string('pickup_vehicle')->nullable();
            $table->string('status')->default(0);
            $table->json('transfer_quotation')->nullable();
            $table->float('transfer_price')->nullable();
            $table->float('transfer_km')->nullable();
            $table->float('grand_total')->nullable();
            $table->float('products_price')->nullable();
            $table->time('transfer_disposal')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
