<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecialpointVehicleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specialpoint_vehicle', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->integer('specialpoint_id')->nullable()->index();
            $table->integer('vehicle_id')->nullable()->index();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specialpoint_vehicle');
    }
}
