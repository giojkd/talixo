<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CerateVouchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();

            $table->string('client_name')->nullable();
            $table->string('client_surname')->nullable();
            $table->string('client_email')->nullable();
            $table->string('client_telephone')->nullable();
            $table->string('client_address')->nullable();
            $table->text('notes')->nullable();
            $table->string('payment_method')->nullable();
            $table->float('grand_total')->nullable();
            $table->text('voucher_code')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}
