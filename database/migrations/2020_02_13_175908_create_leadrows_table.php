<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadrowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leadrows', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('lead_id')->index()->nullable();
            $table->integer('product_id')->index()->nullable();
            $table->date('date')->nullable();
            $table->time('time')->nullable();
            $table->integer('adults')->nullable();
            $table->integer('children')->nullable();
            $table->float('price_per_adult')->nullable();
            $table->float('price_per_child')->nullable();
            $table->float('total')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leadrows');
    }
}
