<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            //
            $table->json('name')->nullable();
            $table->json('description')->nullable();

            $table->json('photos')->nullable();

            $table->integer('persons_max')->nullable();
            $table->integer('luggages_max')->nullable();
            $table->integer('pets_max')->nullable();
            $table->integer('sportequipments_max')->nullable();

            $table->float('cost_per_km')->nullable();
            $table->float('cost_per_hour')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            //
        });
    }
}
