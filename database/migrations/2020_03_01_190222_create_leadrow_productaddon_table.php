<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadrowProductaddonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leadrow_productaddon', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('leadrow_id')->nullable()->index();
            $table->integer('productaddon_id')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leadrow_productaddon');
    }
}
