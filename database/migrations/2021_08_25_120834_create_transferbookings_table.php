<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransferbookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transferbookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('vehicle_id')->nullable()->index();

            $table->timestamp('pickup_at')->nullable();

            $table->json('pickup')->nullable();
            $table->json('destination')->nullable();

            $table->integer('duration')->nullable(); #expressed in minutes

            $table->integer('adults')->nullable();
            $table->integer('children')->nullable();
            $table->integer('infants')->nullable();
            $table->integer('luggages')->nullable();
            $table->integer('pets')->nullable();
            $table->integer('sportequipments')->nullable();

            $table->integer('transferbookingstatus_id')->nullable();

            $table->timestamp('confirmed_at')->nullable();

            $table->timestamp('paid_at')->nullable();
            $table->string('pi')->nullable();
            $table->float('total')->nullable();

            $table->timestamp('deleted_at')->nullable();

            $table->string('email')->nullable();
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('prefix')->nullable();
            $table->string('mobile')->nullable();
            $table->text('notes')->nullable();
            $table->string('request_ip')->nullable();

            $table->json('checkboxes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transferbookings');
    }
}
