<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->json('name')->nullable();
            $table->json('short_description')->nullable();
            $table->json('description')->nullable();
            $table->json('address')->nullable();
            $table->double('longitude')->nullable();
            $table->double('latitude')->nullable();
            $table->float('free_transfer_value')->nullable();
            $table->integer('free_transfer_up_to')->nullable();
            $table->boolean('can_be_private')->nullable()->default(0);
            $table->time('duration')->nullable();
            $table->integer('category_id')->nullable()->index();
            $table->json('photos')->nullable();
            $table->boolean('enabled')->nullable()->default(1);
            $table->integer('geofence_id')->nullable()->index();
            $table->json('subtitle')->nullable();
            $table->float('grand_total')->nullable();
            $table->json('whats_included')->nullable();
            $table->json('whats_not_included')->nullable();
            $table->boolean('interstitial_page_after_adding_to_cart')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

#ALTER TABLE `products` CHANGE `free_transfer_percent` `free_transfer` DOUBLE(8,2) NULL DEFAULT NULL;
#ALTER TABLE `products` ADD `free_transfer_up_to` INT NULL DEFAULT NULL AFTER `free_transfer`;
