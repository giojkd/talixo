<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('category', 'CategoryCrudController');
    Route::crud('product', 'ProductCrudController');
    Route::crud('availability', 'AvailabilityCrudController');
    Route::crud('geofence', 'GeofenceCrudController');
    Route::crud('review', 'ReviewCrudController');
    Route::crud('language', 'LanguageCrudController');
    Route::crud('article', 'ArticleCrudController');
    Route::crud('lead', 'LeadCrudController');
    Route::crud('leadrow', 'LeadrowCrudController');
    Route::crud('vehicle', 'VehicleCrudController');
    Route::crud('menu', 'MenuCrudController');
    Route::crud('textblock', 'TextblockCrudController');
    Route::crud('slideshow', 'SlideshowCrudController');
    Route::crud('productaddon', 'ProductaddonCrudController');
    Route::crud('pricerule', 'PriceruleCrudController');
    Route::crud('voucher', 'VoucherCrudController');
    Route::crud('event', 'EventCrudController');

    Route::get('availability-bulk','AvailabilityCrudController@availabilityBulkGet')->name('availabilityBulkGet');
    Route::post('availability-bulk','AvailabilityCrudController@availabilityBulkCreate')->name('availabilityBulkCreate');
    Route::crud('specialpoint', 'SpecialpointCrudController');
    Route::crud('specialpointpriceoption', 'SpecialpointpriceoptionCrudController');
    Route::crud('transferbooking', 'TransferbookingCrudController');
}); // this should be the absolute last line of this file