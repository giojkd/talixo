<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
return view('welcome');
});
*/
Route::group(['middleware'=>'language'],function (){
  Route::get('/newsletter-subscription','FrontController@newsletterSubscription')->name('newsletterSubscription');

  Route::get('/map','FrontController@map')->name('map');

  Route::get('/','FrontController@home')->name('home');
  Route::get('/cat/{name}/{id}','FrontController@category')->name('category');
  Route::get('/prod/{name}/{id}','FrontController@product')->name('product');
  Route::get('/art/{name}/{id}','FrontController@article')->name('article');

  Route::post('/calculate-transfer-cost/','FrontController@calculateTransferCost')->name('calculateTransferCost');
  Route::get('/checkout','FrontController@checkout')->name('checkout');
  Route::post('/pay','FrontController@pay')->name('pay');



  Route::post('/add-to-cart','FrontController@addToCart')->name('addToCart');
  Route::get('/remove-from-cart/{id}','FrontController@removeFromCart')->name('removeFromCart');

  Route::get('/thank-you/{id}','FrontController@thankYou')->name('thankYou');

  Route::get('test-distance-calculator/{from}/{to}','RoadController@calculateDistanceBetweenTwoPoints');

  Route::get('contact-us','FrontController@contactUs')->name('contactUs');
  Route::get('experiences','FrontController@experiences')->name('experiences');

  Route::get('set-language/{language}','FrontController@setLanguage')->name('languageSwitcher');

    Route::get('vouchers','FrontController@vouchers')->name('vouchers');
    Route::post('voucher-checkout','FrontController@voucherCheckout')->name('voucherCheckout');

    Route::get('interstitial-page-afterr-adding-to-card/{id}','FrontController@interstitialPageAfterAddingToCart')->name('interstitialPageAfterAddingToCart');


});


Route::post('/admin/media-dropzone', ['uses' => 'MediaController@handleDropzoneUpload']);
