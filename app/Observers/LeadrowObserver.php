<?php

namespace App\Observers;

use App\Models\Leadrow;

class LeadrowObserver
{
    /**
     * Handle the leadrow "created" event.
     *
     * @param  \App\Models\Leadrow $Leadrow
     * @return void
     */
    public function created(Leadrow $Leadrow)
    {
        //
        //$Leadrow->calculateLeadTotals();
    }

    /**
     * Handle the leadrow "updated" event.
     *
     * @param  \App\Models\Leadrow $Leadrow
     * @return void
     */
    public function updated(Leadrow $Leadrow)
    {
        //
        $Leadrow->lead->calculateLeadTotals();
    }

    /**
     * Handle the leadrow "deleted" event.
     *
     * @param  \App\Models\Leadrow $Leadrow
     * @return void
     */
    public function deleted(Leadrow $Leadrow)
    {
        //
        $Leadrow->lead->calculateLeadTotals();
    }

    /**
     * Handle the leadrow "restored" event.
     *
     * @param  \App\Models\Leadrow $Leadrow
     * @return void
     */
    public function restored(Leadrow $Leadrow)
    {
        //
    }

    /**
     * Handle the leadrow "force deleted" event.
     *
     * @param  \App\Models\Leadrow $Leadrow
     * @return void
     */
    public function forceDeleted(Leadrow $Leadrow)
    {
        //
    }
}
