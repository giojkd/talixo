<?php

namespace App\Models;

use Mail;
use App\Mail\OrderEmail;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Helpers;

class Lead extends Model
{
    use CrudTrait;
    use Helpers;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'leads';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $casts = [
        #'transfer_disposal' => 'date',
    ];
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function sendConfirmation(){
        $lead = $this;
        $data = [
            'content' => [
                'lead_id' => $lead->id,
                'pickup_address' => $lead->pickup_address,
                'client_name' => $lead->client_name . ' ' . $lead->client_surname,
                'placed_at' => $lead->created_at->format('d F Y'),
                'client_address' => $lead->client_address,
                'client_email' => $lead->client_email,
                'rows' => $lead->leadrows,
                'site_url' => 'https://www.talixotuscany.com/',
                'subtotal' => $lead->grand_total - $lead->transfer_price,
                'transfer' => $lead->transfer_price,
                'grand_total' => $lead->grand_total,
                'company_info_footer' => 'Talixo Tuscany srl'
            ]
        ];

        #dd($data);

        Mail::to($lead->client_email)->send(new OrderEmail($data));
    }


    public function calculateTransportationCost()
    {
        $costPerKm = 1.5;
        $minCost = 30;
        $disposalHourlyPrice = 45;

        $rows = $this->leadrows;
        $disposalDuration = 0;
        if ($this->pickup_address != '') {
            if (!is_null($rows)) {
                $segments = [];
                $points = [];
                $minutes = 0;
                $meters = 0;
                $points[] = [0 => (float)$this->pickup_address_latitude, 1 => (float)$this->pickup_address_longitude]; #PICK UP PLACE
                foreach ($rows as $row) {
                    $disposalDuration+= $this->time_to_decimal($row->product->duration);
                    $points[] = [$row->product->latitude, $row->product->longitude];
                    $minutes += $this->time_to_decimal($row->product->duration);
                }



                $points[] = $points[0]; #DROP PLACE (SAME AS PICKUP)
                for ($i = 0; $i < (count($points) - 1); $i++) {
                    $segments[] = [$points[$i], $points[($i + 1)]];
                }

                foreach ($segments as $key => $segment) {
                    $distance[] = Helpers::calculateDistanceBetweenTwoPoints($segment[0], $segment[1]);
                }
                foreach ($distance as $key => $item) {
                    #$disposalDuration-=$item['duration'] / 60;
                    $minutes += ceil($item['duration'] / 60);
                    $meters += $item['distance'];
                    #$disposalDuration-=ceil($item['duration']);
                }
            }

            $quotedPrice = $costPerKm * $meters / 1000;

            $price = ($quotedPrice > $minCost) ? $quotedPrice : $minCost;



            $disposalPrice = $disposalDuration/60 * $disposalHourlyPrice;



            $price += $disposalPrice;

            #$price-=$minCost;


            $return = [
                'status' => 1,
                'segments' => $distance,
                'totals' => [
                    'minutes' => $minutes,
                    'meters' => $meters,
                ],
                'disposalPrice ' => (float)number_format($disposalPrice,2),
                'quotedPrice' => (float)number_format($quotedPrice, 2),
                'price' => (float)number_format($price, 2),
                'costPerKm' => $costPerKm,
                'minCost' => $minCost,
                'disposalDuration' => $disposalDuration,
                'disposalHourlyPrice' => $disposalHourlyPrice
            ];

        } else {
            $return = [
                'status' => 0
            ];
        }
        return $return;
    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function leadrows()
    {
        return $this->hasMany('App\Models\Leadrow');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
