<?php

namespace App\Models;

use Mail;
use App\Mail\VoucherEmail;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'vouchers';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function sendConfirmation(){
        $voucher = $this;
        $data = [
            'content' => [
                'voucher_id' => $voucher->id,
                'client_name' => $voucher->client_name . ' ' . $voucher->client_surname,
                'placed_at' => $voucher->created_at->format('d F Y'),
                'client_address' => $voucher->client_address,
                'client_email' => $voucher->client_email,
                'site_url' => 'https://www.talixotuscany.com/',
                'grand_total' => $voucher->grand_total,
                'company_info_footer' => 'Talixo Tuscany srl',
                'voucher_code' => $voucher->voucher_code
            ]
        ];

        #dd($data);

        Mail::to($voucher->client_email)->send(new VoucherEmail($data));
    }

    public function generateCode(){
        $this->voucher_code = md5($this->client_surname.'tlx'.$this->created_at.'money'.$this->grand_total);
        $this->save();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
