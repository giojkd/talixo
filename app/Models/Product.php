<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;;
use App\Traits\Helpers;

class Product extends Model
{
  use CrudTrait;
  use HasTranslations;
  use Helpers;
  /*
  |--------------------------------------------------------------------------
  | GLOBAL VARIABLES
  |--------------------------------------------------------------------------
  */

  protected $table = 'products';
  // protected $primaryKey = 'id';
  // public $timestamps = false;
  protected $guarded = ['id'];
  public $translatable = ['name','short_description','description','subtitle','whats_included','whats_not_included'];
  // protected $fillable = [];
  // protected $hidden = [];
  // protected $dates = [];

  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  public function getCover(){
      return $this->getPhotos()->first();
  }

  public function getPhotos(){
    return collect(array_values(json_decode($this->photos,1)));
  }
  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */
  public function category(){
    return $this->belongsTo('App\Models\Category');
  }
  public function geofence(){
    return $this->belongsTo('App\Models\Geofence');
  }
  public function availabilities(){
    return $this->hasMany('App\Models\Availability');
  }
  public function reviews(){
    return $this->hasMany('App\Models\Review');
  }
  public function languages(){
    return $this->belongsToMany('App\Models\Language');
  }
  public function addons(){
    return $this->hasMany('App\Models\Productaddon');
  }
  public function pricerules(){
    return $this->hasMany('App\Models\Pricerule');
  }
  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */
  public function getPhotosAttribute($value){

    $value = json_decode($value);

    return json_encode(collect($value)->map(function($photo){
      return 'storage/'.str_replace('storage/','',$photo);
    })->toArray());

  }
  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */

  public function setPhotosAttribute($value)
  {

    $attribute_name = "photos";
    $disk = "custom_public";
    $destination_path = "";

    $this->uploadMultipleFilesToDisk($value, $attribute_name, $disk, $destination_path);

  }



  protected $casts = [
    'photos' => 'array'
  ];

}
