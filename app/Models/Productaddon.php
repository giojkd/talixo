<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Str;

class Productaddon extends Model
{
    use CrudTrait;
    use HasTranslations;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'productaddons';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];

    public $translatable = ['name','description'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function product(){
      return $this->belongsTo('App\Models\Product');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setCoverAttribute($value)
    {
      $attribute_name = "cover";
      $disk = config('backpack.base.root_disk_name');
      $destination_path = "public/storage";

      if ($value==null) {
        \Storage::disk($disk)->delete($this->{$attribute_name});
        $this->attributes[$attribute_name] = null;
      }

      if (starts_with($value, 'data:image'))
      {
        $image = \Image::make($value)->encode('jpg', 90);
        $filename = md5($value.time()).'.jpg';
        \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
        $public_destination_path = Str::replaceFirst('public/', '', $destination_path);
        $this->attributes[$attribute_name] = $public_destination_path.'/'.$filename;
      }
    }
}
