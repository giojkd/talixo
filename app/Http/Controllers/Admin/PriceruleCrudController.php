<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PriceruleRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
* Class PriceruleCrudController
* @package App\Http\Controllers\Admin
* @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
*/
class PriceruleCrudController extends CrudController
{
  use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

  public function setup()
  {
    $this->crud->setModel('App\Models\Pricerule');
    $this->crud->setRoute(config('backpack.base.route_prefix') . '/pricerule');
    $this->crud->setEntityNameStrings('pricerule', 'pricerules');
  }

  protected function setupListOperation()
  {
    // TODO: remove setFromDb() and manually define Columns, maybe Filters
    #$this->crud->setFromDb();


    $this->crud->addColumn([  // Select2
      'label' => "Product",
      'type' => 'select',
      'name' => 'product_id', // the db column for the foreign key
      'entity' => 'product', // the method that defines the relationship in your Model
      'attribute' => 'name', // foreign key attribute that is shown to user
      'model' => "App\Models\Product", // foreign key model

    ]);


    $this->crud->addColumn([
      'label' => 'From',
      'name' => 'from_participants',
      'type' => 'text'
    ]);


    $this->crud->addColumn([
      'label' => 'To',
      'name' => 'to_participants',
      'type' => 'text'
    ]);


    $this->crud->addColumn([
      'label' => 'Discount',
      'name' => 'discount',
      'type' => 'text',
      //'prefix' => "€",
      'suffix' => "%",
    ]);

  }

  protected function setupCreateOperation()
  {
    $this->crud->setValidation(PriceruleRequest::class);

    // TODO: remove setFromDb() and manually define Fields
    #$this->crud->setFromDb();
    $this->crud->addField([  // Select2
      'label' => "Product",
      'type' => 'select2',
      'name' => 'product_id', // the db column for the foreign key
      'entity' => 'product', // the method that defines the relationship in your Model
      'attribute' => 'name', // foreign key attribute that is shown to user
      'model' => "App\Models\Product", // foreign key model

    ]);


    $this->crud->addField([
      'label' => 'From',
      'name' => 'from_participants',
      'type' => 'text'
    ]);


    $this->crud->addField([
      'label' => 'To',
      'name' => 'to_participants',
      'type' => 'text'
    ]);


    $this->crud->addField([
      'label' => 'Discount',
      'name' => 'discount',
      'type' => 'text',
      //'prefix' => "€",
      'suffix' => "%",
    ]);
  }

  protected function setupUpdateOperation()
  {
    $this->setupCreateOperation();
  }
}
