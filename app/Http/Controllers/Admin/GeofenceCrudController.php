<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\GeofenceRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class GeofenceCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class GeofenceCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
      use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;

      protected function setupReorderOperation()
      {
        // define which model attribute will be shown on draggable elements
        $this->crud->set('reorder.label', 'name');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', 2);
      }

    public function setup()
    {
        $this->crud->setModel('App\Models\Geofence');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/geofence');
        $this->crud->setEntityNameStrings('geofence', 'geofences');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn([
          'name' => 'name', // The db column name
          'label' => "Name", // Table column heading
          'type' => 'text'
        ]);
        $this->crud->addColumn([
          'name' => 'slug', // The db column name
          'label' => "Slug", // Table column heading
          'type' => 'text'
        ]);
        $this->crud->addColumn([
          'name' => 'depth', // The db column name
          'label' => "Depth", // Table column heading
          'type' => 'depth'
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(GeofenceRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
        $this->crud->addField([   // Text
          'name' => 'name',
          'label' => "Name",
          'type' => 'text',
        ]);
        $this->crud->addField([   // Text
          'name' => 'slug',
          'label' => "Slug",
          'type' => 'text',
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();

    }
}
