<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CategoryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
* Class CategoryCrudController
* @package App\Http\Controllers\Admin
* @property-read CrudPanel $crud
*/
class CategoryCrudController extends CrudController
{
  use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;

  protected function setupReorderOperation()
  {
    // define which model attribute will be shown on draggable elements
    $this->crud->set('reorder.label', 'name');
    // define how deep the admin is allowed to nest the items
    // for infinite levels, set it to 0
    $this->crud->set('reorder.max_level', 2);
  }

  public function setup()
  {
    $this->crud->setModel('App\Models\Category');
    $this->crud->setRoute(config('backpack.base.route_prefix') . '/category');
    $this->crud->setEntityNameStrings('category', 'categories');
  }

  protected function setupListOperation()
  {
    // TODO: remove setFromDb() and manually define Columns, maybe Filters
    #$this->crud->setFromDb();
    $this->crud->addColumn([
      'name' => 'name', // The db column name
      'label' => "Name", // Table column heading
      'type' => 'text'
    ]);
    $this->crud->addColumn([
      'name' => 'depth', // The db column name
      'label' => "Depth", // Table column heading
      'type' => 'depth'
    ]);
  }

  protected function setupCreateOperation()
  {
    $this->crud->setValidation(CategoryRequest::class);

    // TODO: remove setFromDb() and manually define Fields
    #$this->crud->setFromDb();
    $this->crud->addField([   // Text
      'name' => 'name',
      'label' => "Name",
      'type' => 'text',

      // optional
      //'prefix' => '',
      //'suffix' => '',
      //'default'    => 'some value', // default value
      //'hint'       => 'Some hint text', // helpful text, show up after input
      //'attributes' => [
      //'placeholder' => 'Some text when empty',
      //'class' => 'form-control some-class'
      //], // extra HTML attributes and values your input might need
      //'wrapperAttributes' => [
      //'class' => 'form-group col-md-12'
      //], // extra HTML attributes for the field wrapper - mostly for resizing fields
      //'readonly'=>'readonly',
    ]);

    $this->crud->addField([
      'label' => 'Description',
      'name' => 'description',
      'type' => 'summernote'
    ]);

    $this->crud->addField([
      'label' => "Cover",
      'name' => "cover",
      'type' => 'image',
      'upload' => true,
      'crop' => true, // set to true to allow cropping, false to disable
      //'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
      // 'disk' => 's3_bucket', // in case you need to show images from a different disk
      // 'prefix' => 'uploads/images/profile_pictures/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
    ]);

  }

  protected function setupUpdateOperation()
  {
    $this->setupCreateOperation();
  }
}
