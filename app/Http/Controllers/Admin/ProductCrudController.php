<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
* Class ProductCrudController
* @package App\Http\Controllers\Admin
* @property-read CrudPanel $crud
*/
class ProductCrudController extends CrudController
{
  use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

  public function setup()
  {
    $this->crud->setModel('App\Models\Product');
    $this->crud->setRoute(config('backpack.base.route_prefix') . '/product');
    $this->crud->setEntityNameStrings('product', 'products');
  }

  protected function setupListOperation()
  {
    // TODO: remove setFromDb() and manually define Columns, maybe Filters
    #$this->crud->setFromDb();
    $this->crud->addColumn([
      'label' => 'ID',
      'name' => 'id',
      'type' => 'text'
    ]);
    $this->crud->addColumn([
      'label' => 'Name',
      'name' => 'name',
      'type' => 'text'
    ]);
    $this->crud->addColumn([  // Select2
      'label' => "Geofence",
      'type' => 'select',
      'name' => 'geofence_id', // the db column for the foreign key
      'entity' => 'geofence', // the method that defines the relationship in your Model
      'attribute' => 'name', // foreign key attribute that is shown to user
      'model' => "App\Models\Geofence", // foreign key model

    ]);
    $this->crud->addColumn([
      'label' => 'Enabled',
      'name' => 'enabled',
      'type' => 'check',
    ]);
    $this->crud->addColumn([
      'label' => 'Free Transfer',
      'name' => 'free_transfer',
      'type' => 'text'
    ]);
    $this->crud->addColumn([
      'label' => 'Up To',
      'name' => 'free_transfer_up_to',
      'type' => 'text'
    ]);
  }

  protected function setupCreateOperation()
  {
    $this->crud->setValidation(ProductRequest::class);

    // TODO: remove setFromDb() and manually define Fields
    #$this->crud->setFromDb();

    $this->crud->addField([
      'label' => 'Enabled',
      'name' => 'enabled',
      'type' => 'checkbox',
    ]);

      $this->crud->addField([
          'label' => 'Interstitial Page',
          'name' => 'interstitial_page_after_adding_to_cart',
          'type' => 'checkbox',
      ]);

    $this->crud->addField([
      'label' => 'Name',
      'name' => 'name',
      'type' => 'text'
    ]);
    $this->crud->addField([
      'label' => 'Subtitle',
      'name' => 'subtitle',
      'type' => 'text'
    ]);
    $this->crud->addField([
      'label' => 'Short description',
      'name' => 'short_description',
      'type' => 'textarea'
    ]);

    $this->crud->addField([
      'label' => 'Description',
      'name' => 'description',
      'type' => 'tinymce'
    ]);

    $this->crud->addField([
      'label' => 'What is included',
      'name' => 'whats_included',
      'type' => 'tinymce'
    ]);

    $this->crud->addField([
      'label' => 'What is not included',
      'name' => 'whats_not_included',
      'type' => 'tinymce'
    ]);

    $this->crud->addField([
      'label' => 'Address',
      'name' => 'address',
      'type' => 'address_algolia',
      'store_as_json' => true
    ]);

    $this->crud->addField([
      'label' => 'Available as private tour',
      'name' => 'can_be_private',
      'type' => 'checkbox',
    ]);

    $this->crud->addField([
      'label' => 'Duration',
      'name' => 'duration',
      'type' => 'time',
    ]);

    $this->crud->addField([
      'label' => 'Free transfer',
      'name' => 'free_transfer',
      'prefix' => "€",
      'type' => 'number',
      'suffix' => ".00",
    ]);

    $this->crud->addField([
      'label' => 'Transfer up to',
      'name' => 'free_transfer_up_to',
      'type' => 'radio',
      'inline' => true,
      'options'     => [
        // the key will be stored in the db, the value will be shown as label;
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
        6 => 6,
        7 => 7,
        8 => 8,
        9 => 9,
        10 => 10,
      ],
    ]);

    $this->crud->addField([  // Select2
      'label' => "Category",
      'type' => 'select2_nested',
      'name' => 'category_id', // the db column for the foreign key
      'entity' => 'category', // the method that defines the relationship in your Model
      'attribute' => 'name', // foreign key attribute that is shown to user
      'model' => "App\Models\Category", // foreign key model

    ]);

    $this->crud->addField([  // Select2
      'label' => "Geofence",
      'type' => 'select2_nested',
      'name' => 'geofence_id', // the db column for the foreign key
      'entity' => 'geofence', // the method that defines the relationship in your Model
      'attribute' => 'name', // foreign key attribute that is shown to user
      'model' => "App\Models\Geofence", // foreign key model

    ]);

    $this->crud->addField([   // Upload
      'name' => 'photos',
      'label' => 'Photos',
      'type' => 'upload_multiple',
      'upload' => true,
      //'disk' => 'public', // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
      // optional:
      //'temporary' => 10 // if using a service, such as S3, that requires you to make temporary URL's this will make a URL that is valid for the number of minutes specified
    ]);


    $this->crud->addField(
      [   // SelectMultiple = n-n relationship (with pivot table)
        'label' => "Languages",
        'type' => 'select2_multiple',
        'name' => 'languages', // the method that defines the relationship in your Model
        'entity' => 'tags', // the method that defines the relationship in your Model
        'attribute' => 'name', // foreign key attribute that is shown to user
        'model' => "App\Models\Language", // foreign key model
        'pivot' => true, // on create&update, do you need to add/delete pivot table entries?

        // optional
        #'options'   => (function ($query) {
        #  return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
        #}), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
      ]
    );

  }

  protected function setupUpdateOperation()
  {
    $this->setupCreateOperation();
  }
}
