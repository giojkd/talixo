<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductaddonRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ProductaddonCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductaddonCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;
    
    public function setup()
    {
        $this->crud->setModel('App\Models\Productaddon');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/productaddon');
        $this->crud->setEntityNameStrings('productaddon', 'productaddons');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        $this->crud->addColumn([  // Select2
          'label' => "Product",
          'type' => 'select',
          'name' => 'product_id', // the db column for the foreign key
          'entity' => 'product', // the method that defines the relationship in your Model
          'attribute' => 'name', // foreign key attribute that is shown to user
          'model' => "App\Models\Product", // foreign key model
        ]);

        $this->crud->addColumn([
          'label' => 'Name',
          'name' => 'name',
          'type' => 'text'
        ]);

        $this->crud->addColumn([
          'label' => 'Price per adult',
          'name' => 'price_per_adult',
          'type' => 'number',
        ]);

        $this->crud->addColumn([
          'label' => 'Price per child',
          'name' => 'price_per_child',
          'type' => 'number',
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ProductaddonRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addField([  // Select2
          'label' => "Product",
          'type' => 'select2',
          'name' => 'product_id', // the db column for the foreign key
          'entity' => 'product', // the method that defines the relationship in your Model
          'attribute' => 'name', // foreign key attribute that is shown to user
          'model' => "App\Models\Product", // foreign key model

        ]);

        $this->crud->addField([
          'label' => 'Name',
          'name' => 'name',
          'type' => 'text'
        ]);

        $this->crud->addField([
          'label' => 'Description',
          'name' => 'description',
          'type' => 'tinymce'
        ]);

        $this->crud->addField([
          'label' => "Cover",
          'name' => "cover",
          'type' => 'image',
          'upload' => true,
          'crop' => true, // set to true to allow cropping, false to disable
          //'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
          // 'disk' => 's3_bucket', // in case you need to show images from a different disk
          // 'prefix' => 'uploads/images/profile_pictures/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
        ]);

        $this->crud->addField([
          'label' => 'Price per adult',
          'name' => 'price_per_adult',
          'type' => 'number',
          'attributes' => ['min' => 0],
          'prefix' => "€",
          'suffix' => ".00",
        ]);

        $this->crud->addField([
          'label' => 'Price per child',
          'name' => 'price_per_child',
          'type' => 'number',
          'attributes' => ['min' => 0],
          'prefix' => "€",
          'suffix' => ".00",
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
