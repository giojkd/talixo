<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AvailabilityRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\Product;
use App\Traits\Helpers;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Availability;
use Str;

/**
* Class AvailabilityCrudController
* @package App\Http\Controllers\Admin
* @property-read CrudPanel $crud
*/
class AvailabilityCrudController extends CrudController
{
  use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;


  use Helpers;

  public function setup()
  {
    $this->crud->setModel('App\Models\Availability');
    $this->crud->setRoute(config('backpack.base.route_prefix') . '/availability');
    $this->crud->setEntityNameStrings('availability', 'availabilities');
  }

  protected function setupListOperation()
  {
    // TODO: remove setFromDb() and manually define Columns, maybe Filters
    #$this->crud->setFromDb();#
    $this->crud->addColumn([  // Select2
      'label' => "Product",
      'type' => 'select',
      'name' => 'product_id', // the db column for the foreign key
      'entity' => 'product', // the method that defines the relationship in your Model
      'attribute' => 'name', // foreign key attribute that is shown to user
      'model' => "App\Models\Product", // foreign key model
    ]);
    $this->crud->addColumn([
      'label' => 'Day',
      'name' => 'day',
      'type' => 'date'
    ]);
    $this->crud->addColumn([
      'label' => 'Time',
      'name' => 'time',
      'type' => 'time',
      'default' => '09:30'
    ]);
    $this->crud->addColumn([
      'label' => 'Price per adult',
      'name' => 'price_per_adult',
      'type' => 'number',
      'attributes' => ['min' => 0],
      'prefix' => "€",
      'suffix' => ".00",
    ]);

    $this->crud->addColumn([
      'label' => 'Price per child',
      'name' => 'price_per_child',
      'type' => 'number',
      'attributes' => ['min' => 0],
      'prefix' => "€",
      'suffix' => ".00",
    ]);

    $this->crud->addColumn([
      'label' => 'Quantity',
      'name' => 'quantity',
      'type' => 'number',
      'attributes' => ['min' => 1],
      'default' => 1
    ]);
  }

  protected function setupCreateOperation()
  {
    $this->crud->setValidation(AvailabilityRequest::class);

    // TODO: remove setFromDb() and manually define Fields
    #$this->crud->setFromDb();

    $this->crud->addField([  // Select2
      'label' => "Product",
      'type' => 'select2',
      'name' => 'product_id', // the db column for the foreign key
      'entity' => 'product', // the method that defines the relationship in your Model
      'attribute' => 'name', // foreign key attribute that is shown to user
      'model' => "App\Models\Product", // foreign key model
    ]);

    $this->crud->addField([
      'label' => 'Day',
      'name' => 'day',
      'type' => 'date_picker'
    ]);
    $this->crud->addField([
      'label' => 'Time',
      'name' => 'time',
      'type' => 'time',
      'default' => '09:30'
    ]);

    $this->crud->addField([
      'label' => 'Price per adult',
      'name' => 'price_per_adult',
      'type' => 'number',
      'attributes' => ['min' => 0],
      'prefix' => "€",
      'suffix' => ".00",
    ]);

    $this->crud->addField([
      'label' => 'Price per child',
      'name' => 'price_per_child',
      'type' => 'number',
      'attributes' => ['min' => 0],
      'prefix' => "€",
      'suffix' => ".00",
    ]);

    $this->crud->addField([
      'label' => 'Quantity',
      'name' => 'quantity',
      'type' => 'number',
      'attributes' => ['min' => 1],
      'default' => 1
    ]);




    $this->crud->addField([
      'label' => 'Min Adv Notice',
      'name' => 'min_adv_notice',
      'type' => 'number',
      'attributes' => ['min' => 0],
      'default' => 1
    ]);

    $this->crud->addField([
      'label' => 'Min Persons',
      'name' => 'min_persons',
      'type' => 'number',
      'attributes' => ['min' => 0],
      'default' => 1
    ]);

    $this->crud->addField([
      'label' => 'Max Persons',
      'name' => 'max_persons',
      'type' => 'number',
      'attributes' => ['min' => 0],
      'default' => 100
    ]);





  }

  protected function setupUpdateOperation()
  {
    $this->setupCreateOperation();
  }

  public function availabilityBulkCreate(Request $request){
    $data = $request->all();



    $lastDay = Carbon::createFromFormat('Y-m-d',Str::limit($data['end_date'],10,''));
    $day = Carbon::createFromFormat('Y-m-d',Str::limit($data['start_date'],10,''));
    while($day->lte($lastDay)){

      if(in_array($day->dayOfWeekIso,$data['days_of_the_week'])){
        foreach($data['time_options'] as $timeOption){
          foreach($data['product_id'] as $productId){
            $availability = Availability::firstOrCreate([
              'product_id' => $productId,
              'time' => $timeOption,
              'day' => $day->format('Y-m-d')
            ]);
            $availability->fill([
              'price_per_adult' => $data['price_per_adult'],
              'price_per_child' => $data['price_per_child'],
              'quantity' => $data['quantity'],
              'min_adv_notice' => $data['min_adv_notice'],
              'min_persons' => $data['min_persons'],
              'max_persons' => $data['max_persons']
            ]);
            $availability->save();
          }
        }
      }

      $day = $day->addDay();
    }

    return redirect('/admin/availability');



  }

  public function availabilityBulkGet(){

    $this->crud->setValidation(AvailabilityRequest::class);

    $this->crud->addField([  // Select2
      'label' => "Product",
      'type' => 'select2_multiple',
      'name' => 'product_id', // the db column for the foreign key
      'entity' => 'product', // the method that defines the relationship in your Model
      'attribute' => 'name', // foreign key attribute that is shown to user
      'model' => "App\Models\Product", // foreign key model

    ]);

    $this->crud->addField([   // date_range
      'name' => ['start_date', 'end_date'], // db columns for start_date & end_date
      'label' => 'Date Range',
      'type' => 'date_range',
      // OPTIONALS
      'default' => [date('Y-m-d'), date('Y-m-d',strtotime('+1months'))], // default values for start_date & end_date
      'date_range_options' => [
        // options sent to daterangepicker.js
        'timePicker' => false,
        'locale' => ['format' => 'DD/MM/YYYY']
      ]
    ]);

    $timeOptions = [];

    for($i = 0; $i<23; $i++){
      for($j = 0; $j<60; $j = $j+5){
        $timeOption =  $this->addZ($i).':'.$this->addZ($j);
        $timeOptions[$timeOption] = $timeOption;
      }
    }

    $this->crud->addField([
      'name' => 'time_options',
      'label' => "Time options",
      'type' => 'select2_from_array',
      'options' => $timeOptions,
      'allows_null' => false,
      'allows_multiple' => true,
    ]);

    $daysOfTheWeek = [
      1 => 'Monday',
      2 => 'Tuesday',
      3 => 'Wednesday',
      4 => 'Thursday',
      5 => 'Friday',
      6 => 'Saturday',
      7 => 'Sunday',
    ];

    $this->crud->addField([
      'name' => 'days_of_the_week',
      'label' => "Days of the week",
      'type' => 'select2_from_array',
      'options' => $daysOfTheWeek,
      'allows_null' => false,
      'allows_multiple' => true,
    ]);

    $this->crud->addField([
      'label' => 'Price per adult',
      'name' => 'price_per_adult',
      'type' => 'number',
      'attributes' => ['min' => 0],
      'prefix' => "€",
      'suffix' => ".00",
    ]);

    $this->crud->addField([
      'label' => 'Price per child',
      'name' => 'price_per_child',
      'type' => 'number',
      'attributes' => ['min' => 0],
      'prefix' => "€",
      'suffix' => ".00",
    ]);

    $this->crud->addField([
      'label' => 'Quantity',
      'name' => 'quantity',
      'type' => 'number',
      'attributes' => ['min' => 1],
      'default' => 1
    ]);


    $this->crud->addField([
      'label' => 'Min Adv Notice',
      'name' => 'min_adv_notice',
      'type' => 'number',
      'attributes' => ['min' => 0],
      'default' => 1
    ]);

    $this->crud->addField([
      'label' => 'Min Persons',
      'name' => 'min_persons',
      'type' => 'number',
      'attributes' => ['min' => 0],
      'default' => 1
    ]);

    $this->crud->addField([
      'label' => 'Max Persons',
      'name' => 'max_persons',
      'type' => 'number',
      'attributes' => ['min' => 0],
      'default' => 100
    ]);



    return view('backend.availability_bulk',['crud' => $this->crud]);
  }

}
