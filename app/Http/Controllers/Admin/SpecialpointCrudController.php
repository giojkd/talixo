<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SpecialpointRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SpecialpointCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SpecialpointCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Specialpoint');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/specialpoint');
        $this->crud->setEntityNameStrings('specialpoint', 'specialpoints');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
     #   $this->crud->setFromDb();

     $this->crud->addColumns([
            [
                'label' => 'Nome',
                'name' => 'name',
                'type' => 'text'
            ],
            [
                'label' => 'Label',
                'name' => 'label',
                'type' => 'text'
            ],
            [
                'label' => 'Radius',
                'name' => 'radius',
                'type' => 'text',
                'suffix' => 'km'
            ],
            [
                    'label' => 'Type',
                    'type' => 'select_from_array',
                    'name' => 'type',
                    'options' =>
                    ['destination' => 'Destination', 'pickup' => 'Pickup', 'both' => 'Both']


            ]
        ]);

    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(SpecialpointRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addFields([
            [
                'label' => 'Nome',
                'name' => 'name',
                'type' => 'text'
            ],
            [   // Address
                'name' => 'point',
                'label' => 'Address',
                'type' => 'address_google',
                // optional
                'store_as_json' => true
            ],
            [
                'label' => 'Radius (m)',
                'name' => 'radius',
                'type' => 'number',
                'suffix' => 'km',
                'attributes' => [
                    'step' => 'any'
                    ]
                ],
            [
                'label' => 'Type',
                'type' => 'select_from_array',
                'name' => 'type',
                'options' =>
                    ['destination' => 'Destination', 'pickup'=> 'Pickup','both' => 'Both']

            ],


        ]);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
