<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ReviewRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
* Class ReviewCrudController
* @package App\Http\Controllers\Admin
* @property-read CrudPanel $crud
*/
class ReviewCrudController extends CrudController
{
  use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

  public function setup()
  {
    $this->crud->setModel('App\Models\Review');
    $this->crud->setRoute(config('backpack.base.route_prefix') . '/review');
    $this->crud->setEntityNameStrings('review', 'reviews');
  }

  protected function setupListOperation()
  {
    // TODO: remove setFromDb() and manually define Columns, maybe Filters
    #$this->crud->setFromDb();

    $this->crud->addColumn([  // Select2
      'label' => "Product",
      'type' => 'select',
      'name' => 'product_id', // the db column for the foreign key
      'entity' => 'product', // the method that defines the relationship in your Model
      'attribute' => 'name', // foreign key attribute that is shown to user
      'model' => "App\Models\Product", // foreign key model

    ]);

    $this->crud->addColumn([
      'label' => 'Alias',
      'name' => 'alias',
      'type' => 'text'
    ]);

    $this->crud->addColumn([
      'label' => 'Evaluation',
      'name' => 'evaluation',
      'type' => 'text'
    ]);
  }

  protected function setupCreateOperation()
  {
    $this->crud->setValidation(ReviewRequest::class);

    $this->crud->addField([  // Select2
      'label' => "Product",
      'type' => 'select2',
      'name' => 'product_id', // the db column for the foreign key
      'entity' => 'product', // the method that defines the relationship in your Model
      'attribute' => 'name', // foreign key attribute that is shown to user
      'model' => "App\Models\Product", // foreign key model

    ]);

    // TODO: remove setFromDb() and manually define Fields
    #$this->crud->setFromDb();
    $this->crud->addField([
      'label' => 'Alias',
      'name' => 'alias',
      'type' => 'text'
    ]);
    $this->crud->addField([
      'label' => 'Nationality',
      'name' => 'nationality',
      'type' => 'text'
    ]);
    $this->crud->addField([
      'label' => 'Evaluation',
      'name' => 'evaluation',
      'type' => 'radio',
      'options'     => [
        // the key will be stored in the db, the value will be shown as label;
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
        6 => 6,
        7 => 7,
        8 => 8,
        9 => 9,
        10 => 10,
      ],
      'inline' => true
    ]);
    $this->crud->addField([
      'label' => 'Liked',
      'name' => 'liked',
      'type' => 'textarea'
    ]);
    $this->crud->addField([
      'label' => 'Not Liked',
      'name' => 'not_liked',
      'type' => 'textarea'
    ]);

    $this->crud->addField([
      'label' => "Avatar",
      'name' => "avatar",
      'type' => 'image',
      'upload' => true,
      'crop' => true, // set to true to allow cropping, false to disable
      //'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
      // 'disk' => 's3_bucket', // in case you need to show images from a different disk
      // 'prefix' => 'uploads/images/profile_pictures/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
    ]);
  }

  protected function setupUpdateOperation()
  {
    $this->setupCreateOperation();
  }
}
