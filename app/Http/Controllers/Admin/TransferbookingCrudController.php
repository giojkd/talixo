<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TransferbookingRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class TransferbookingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TransferbookingCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Transferbooking');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/transferbooking');
        $this->crud->setEntityNameStrings('transferbooking', 'transferbookings');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumns([
            [  // Select2
                'label' => "Vehicle",
                'type' => 'select',
                'name' => 'vehicle_id', // the db column for the foreign key
                'entity' => 'vehicle', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => "App\Models\Vehicle", // foreign key model

            ],
            [
                'name' => 'pickup_at',
                'type' => 'datetime',
                'label' => 'Pick up at',
            ],
            [   // Address
                'name' => 'pickup_label',
                'label' => 'Pickup',
                'type' => 'text',
            ],
            [   // Address
                'name' => 'destination_label',
                'label' => 'Destination',
                'type' => 'address_google',
                // optional
                'store_as_json' => true
            ],
            [   // Address
                'name' => 'adults',
                'label' => 'Adults',
                'type' => 'text',

            ],
            [   // Address
                'name' => 'children',
                'label' => 'Children',
                'type' => 'text',

            ],
            [   // Address
                'name' => 'infants',
                'label' => 'Infants',
                'type' => 'text',

            ],
            [   // Address
                'name' => 'luggages',
                'label' => 'Luggages',
                'type' => 'text',

            ],
            [   // Address
                'name' => 'sportequipments',
                'label' => 'Sport Equipments',
                'type' => 'text',

            ],
            [   // Address
                'name' => 'pets',
                'label' => 'Pets',
                'type' => 'text',

            ],
            [   // Address
                'name' => 'confirmed_at',
                'label' => 'Confirmed at',
                'type' => 'datetime_picker',

            ],
            [   // Address
                'name' => 'paid_at',
                'label' => 'Paid at',
                'type' => 'datetime_picker',

            ],
            [   // Address
                'name' => 'grand_total',
                'label' => 'Total',
                'type' => 'text',
                'suffix' => '€',

            ],
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(TransferbookingRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $options = [];

        for($i = 0; $i<=8; $i++){
            $options[$i] = $i;
        }

        $this->crud->addFields([
            [  // Select2
                'label' => "Vehicle",
                'type' => 'select2',
                'name' => 'vehicle_id', // the db column for the foreign key
                'entity' => 'vehicle', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => "App\Models\Vehicle", // foreign key model

            ],
            [
                'name' => 'pickup_at',
                'type' => 'datetime_picker',
                'label' => 'Pick up at',
                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY HH:mm',
                    'language' => 'it'
                ],
                'allows_null' => false,
            ],
            [   // Address
                'name' => 'pickup',
                'label' => 'Pickup',
                'type' => 'address_google',
                // optional
                'store_as_json' => true
            ],
            [   // Address
                'name' => 'destination',
                'label' => 'Destination',
                'type' => 'address_google',
                // optional
                'store_as_json' => true
            ],
            [   // Address
                'name' => 'adults',
                'label' => 'Adults',
                'type' => 'select_from_array',
                'options' => array_slice($options,1,8)
            ],
            [   // Address
                'name' => 'children',
                'label' => 'Children',
                'type' => 'select_from_array',
                'options' => array_slice($options, 0, 2)
            ],
            [   // Address
                'name' => 'infants',
                'label' => 'Infants',
                'type' => 'select_from_array',
                'options' => array_slice($options, 0, 2)
            ],
            [   // Address
                'name' => 'luggages',
                'label' => 'Luggages',
                'type' => 'select_from_array',
                'options' => $options
            ],
            [   // Address
                'name' => 'sportequipments',
                'label' => 'Sport Equipments',
                'type' => 'select_from_array',
                'options' => array_slice($options, 0, 3)
            ],
            [   // Address
                'name' => 'pets',
                'label' => 'Pets',
                'type' => 'select_from_array',
                'options' => array_slice($options, 0, 4)
            ],
            [   // Address
                'name' => 'confirmed_at',
                'label' => 'Confirmed at',
                'type' => 'datetime_picker',

            ],
            [   // Address
                'name' => 'paid_at',
                'label' => 'Paid at',
                'type' => 'datetime_picker',

            ],
            [   // Address
                'name' => 'grand_total',
                'label' => 'Total',
                'type' => 'number',
                'suffix' => '€',
                'attributes' => [
                    'step' => 0.01
                ]

            ],


            /*[
                'name' => '',
                'type' => '',
                'label' => ''
            ],
            [
                'name' => '',
                'type' => '',
                'label' => ''
            ],
            [
                'name' => '',
                'type' => '',
                'label' => ''
            ],
            [
                'name' => '',
                'type' => '',
                'label' => ''
            ],
            [
                'name' => '',
                'type' => '',
                'label' => ''
            ],
            [
                'name' => '',
                'type' => '',
                'label' => ''
            ],*/
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
