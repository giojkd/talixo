<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SpecialpointpriceoptionRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SpecialpointpriceoptionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SpecialpointpriceoptionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Specialpointpriceoption');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/specialpointpriceoption');
        $this->crud->setEntityNameStrings('specialpointpriceoption', 'specialpointpriceoptions');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        $this->crud->addColumns([[  // Select2
                'label' => "Vehicle",
                'type' => 'select',
                'name' => 'vehicle_id', // the db column for the foreign key
                'entity' => 'vehicle', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => "App\Models\Vehicle", // foreign key model

            ],
            [  // Select2
                'label' => "Product",
                'type' => 'select',
                'name' => 'specialpoint_id', // the db column for the foreign key
                'entity' => 'specialpoint', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => "App\Models\Specialpoint", // foreign key model

            ],
            [
                'name' => 'price',
                'type' => 'number',
                'attributes' => [
                    'step' => 'any'
                ],
                'suffix' => '€'
            ],
        ]);

    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(SpecialpointpriceoptionRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();


        $this->crud->addFields([
                [  // Select2
                    'label' => "Vehicle",
                    'type' => 'select2',
                    'name' => 'vehicle_id', // the db column for the foreign key
                    'entity' => 'vehicle', // the method that defines the relationship in your Model
                    'attribute' => 'name', // foreign key attribute that is shown to user
                    'model' => "App\Models\Vehicle", // foreign key model

                ],
                [  // Select2
                    'label' => "Product",
                    'type' => 'select2',
                    'name' => 'specialpoint_id', // the db column for the foreign key
                    'entity' => 'specialpoint', // the method that defines the relationship in your Model
                    'attribute' => 'name', // foreign key attribute that is shown to user
                    'model' => "App\Models\Specialpoint", // foreign key model

                ],
            [
                'name' => 'price',
                'type' => 'number',
                'attributes' => [
                    'step' => 'any'
                ],
                'suffix' => '€'
            ],

        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
