<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TextblockRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class TextblockCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TextblockCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Textblock');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/textblock');
        $this->crud->setEntityNameStrings('textblock', 'textblocks');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn([
          'label' => 'Slug',
          'name' => 'slug',
          'type' => 'text'
        ]);

        $this->crud->addColumn([
          'label' => 'Content',
          'name' => 'content',
          'type' => 'text'
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(TextblockRequest::class);

        // TODO: remove setFromDb() and manually define Fields

        #$this->crud->setFromDb();

        $this->crud->addField([
          'label' => 'Slug',
          'name' => 'slug',
          'type' => 'text'
        ]);

        $this->crud->addField([
          'label' => 'Content',
          'name' => 'content',
          'type' => 'textarea'
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
