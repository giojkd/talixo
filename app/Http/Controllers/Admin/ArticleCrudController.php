<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ArticleRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
* Class ArticleCrudController
* @package App\Http\Controllers\Admin
* @property-read CrudPanel $crud
*/
class ArticleCrudController extends CrudController
{
  use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;


  public function setup()
  {
    $this->crud->setModel('App\Models\Article');
    $this->crud->setRoute(config('backpack.base.route_prefix') . '/article');
    $this->crud->setEntityNameStrings('article', 'articles');
  }

  protected function setupReorderOperation()
   {
       // define which model attribute will be shown on draggable elements
       $this->crud->set('reorder.label', 'name');
       // define how deep the admin is allowed to nest the items
       // for infinite levels, set it to 0
       $this->crud->set('reorder.max_level', 2);
   }

  protected function setupListOperation()
  {
    // TODO: remove setFromDb() and manually define Columns, maybe Filters
    #$this->crud->setFromDb();
    $this->crud->addColumn([
      'name' => 'name', // The db column name
      'label' => "Title", // Table column heading
      'type' => 'text'
    ]);
  }

  protected function setupCreateOperation()
  {
    $this->crud->setValidation(ArticleRequest::class);

    // TODO: remove setFromDb() and manually define Fields
    #$this->crud->setFromDb();
    $this->crud->addField([   // Text
      'name' => 'name',
      'label' => "Title",
      'type' => 'text',
    ]);

    $this->crud->addField([
      'label' => 'Content',
      'name' => 'content',
      'type' => 'tinymce',
        'options' => [
                'extended_valid_elements' => 'script[language|type|src]',
                'plugins' =>  'code print preview paste searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount spellchecker  imagetools media  link contextmenu colorpicker textpattern help'
        ]
    ]);

    $this->crud->addField([
      'label' => "Cover",
      'name' => "cover",
      'type' => 'image',
      'upload' => true,
      'crop' => true, // set to true to allow cropping, false to disable
      //'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
      // 'disk' => 's3_bucket', // in case you need to show images from a different disk
      // 'prefix' => 'uploads/images/profile_pictures/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
    ]);

  }

  protected function setupUpdateOperation()
  {
    $this->setupCreateOperation();
  }
}
