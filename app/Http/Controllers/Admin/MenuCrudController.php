<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MenuRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
* Class MenuCrudController
* @package App\Http\Controllers\Admin
* @property-read CrudPanel $crud
*/
class MenuCrudController extends CrudController
{
  use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

  public function setup()
  {
    $this->crud->setModel('App\Models\Menu');
    $this->crud->setRoute(config('backpack.base.route_prefix') . '/menu');
    $this->crud->setEntityNameStrings('menu', 'menus');
  }

  protected function setupListOperation()
  {
    // TODO: remove setFromDb() and manually define Columns, maybe Filters
    $this->crud->setFromDb();
  }

  protected function setupCreateOperation()
  {
    $this->crud->setValidation(MenuRequest::class);

    // TODO: remove setFromDb() and manually define Fields
    #$this->crud->setFromDb();
    $this->crud->addField([
      'label' => 'Name',
      'name' => 'name',
      'type' => 'text'
    ]);

    $this->crud->addField([
      'label' => 'Slug',
      'name' => 'slug',
      'type' => 'text'
    ]);

    $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
      'label'     => "Articles",
      'type'      => 'select2_multiple',
      'name'      => 'articles', // the method that defines the relationship in your Model
      'entity'    => 'articles', // the method that defines the relationship in your Model
      'attribute' => 'name', // foreign key attribute that is shown to user
      'model'     => "App\Models\Article", // foreign key model
      'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?


    ]);

  }

  protected function setupUpdateOperation()
  {
    $this->setupCreateOperation();
  }
}
