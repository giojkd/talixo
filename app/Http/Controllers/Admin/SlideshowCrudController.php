<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SlideshowRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SlideshowCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SlideshowCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Slideshow');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/slideshow');
        $this->crud->setEntityNameStrings('slideshow', 'slideshows');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn([
          'label' => 'Slug',
          'name' => 'slug',
          'type' => 'text'
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(SlideshowRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addField([
          'label' => 'Slug',
          'name' => 'slug',
          'type' => 'text'
        ]);


            $this->crud->addField([   // Upload
              'name' => 'files',
              'label' => 'Photos',
              'type' => 'upload_multiple',
              'upload' => true,
              //'disk' => 'public', // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
              // optional:
              //'temporary' => 10 // if using a service, such as S3, that requires you to make temporary URL's this will make a URL that is valid for the number of minutes specified
            ]);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
