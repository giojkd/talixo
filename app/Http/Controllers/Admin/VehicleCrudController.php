<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\VehicleRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class VehicleCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class VehicleCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Vehicle');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/vehicle');
        $this->crud->setEntityNameStrings('vehicle', 'vehicles');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        $this->crud->addColumns([[
                'label' => 'Nome',
                'name' => 'name',
                'type' => 'text'
            ],
            /*
            [
                'label' => 'Descrizione',
                'name' => 'description',
                'type' => 'textarea'
            ],
            */
            [
                'label' => 'Passeggeri',
                'name' => 'persons_max',
                'type' => 'number'
            ],
            [
                'label' => 'Bambini',
                'name' => 'children',
                'type' => 'number'
            ],
            [
                'label' => 'Infanti',
                'name' => 'infants',
                'type' => 'number'
            ],
            [
                'label' => 'Bagagli',
                'name' => 'luggages_max',
                'type' => 'number'
            ],
            [
                'label' => 'Animali',
                'name' => 'pets_max',
                'type' => 'number'
            ],
            [
                'label' => 'Equipaggiamento da sport',
                'name' => 'sportequipments_max',
                'type' => 'number'
            ],
            [
                'label' => '€/H',
                'name' => 'cost_per_hour',
                'type' => 'number',
                'attributes' => ["step" => "any"]
            ],
            [
                'label' => '€/km',
                'name' => 'cost_per_km',
                'type' => 'number',
                'attributes' => ["step" => "any"]
            ]
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(VehicleRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addFields([
            [
                'label' => 'Nome',
                'name' => 'name',
                'type' => 'text'
            ],
            [
                'label' => 'Descrizione',
                'name' => 'description',
                'type' => 'textarea'
            ],
            [
                'label' => 'Passeggeri max',
                'name' => 'persons_max',
                'type' => 'number'
            ],
            [
                'label' => 'Bagagli max',
                'name' => 'luggages_max',
                'type' => 'number'
            ],
            [
                'label' => 'Animali max',
                'name' => 'pets_max',
                'type' => 'number'
            ],
            [
                'label' => 'Equipaggiamento da sport max',
                'name' => 'sportequipments_max',
                'type' => 'number'
            ],
            [
                'label' => 'Costo orario per disponibilità',
                'name' => 'cost_per_hour',
                'type' => 'number',
                'attributes' => ["step" => "any"]
            ],
            [
                'label' => 'Costo orario per km',
                'name' => 'cost_per_km',
                'type' => 'number',
                'attributes' => ["step" => "any"]
            ],
            [
                'name' => 'photos', // db column name
                'label' => 'Photos', // field caption
                'type' => 'dropzone', // voodoo magic
                'prefix' => '/', // upload folder (should match the driver specified in the upload handler defined below)
                'upload-url' => '/admin/media-dropzone', // POST route to handle the individual file uploads
            ],
        ]);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
