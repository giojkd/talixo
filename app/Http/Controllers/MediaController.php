<?php

namespace App\Http\Controllers;
use App\Http\Requests\DropzoneRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MediaController extends Controller
{
    //

    static function handleDropzoneUpload(DropzoneRequest $request)
    {
        $disk = "custom_public"; //
        $destination_path = "";
        $destination_path_filename = "storage";
        $file = $request->file('file');
        //dd($file);
        //try{
        $filename = md5($file->getClientOriginalName() . time()) . '.' . $file->getClientOriginalExtension();
        Storage::disk($disk)->put($destination_path . '/' . $filename, file_get_contents($file));
        return response()->json(['success' => true, 'filename' => $destination_path_filename . '/' . $filename]);
        //} catch (\Exception $e) {
        //return response('Unknown error', 412);
        //}
    }
}
