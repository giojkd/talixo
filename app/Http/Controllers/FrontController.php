<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Productaddon;
use App\Models\Article;
use App\Models\Category;
use App\Models\Geofence;
use App\Models\Lead;
use App\Models\Language;
use App\Models\Leadrow;
use App\Models\Voucher;
use App\Models\Menu;
use App\Models\Availability;
use App\Models\Textblock;
use App\Models\Slideshow;
use Mail;
use App\Mail\OrderEmail;
use Cookie;
use Str;
use App;
use Carbon\Carbon;
use Newsletter;
use Illuminate\Support\Facades\Validator;


#use \DrewM\MailChimp\MailChimp;


class FrontController extends Controller
{
    //

    var $cookieDuration = 86400 * 365;

    public function __construct()
    {

        app()->setLocale('en');
    }

    public function newsletterSubscription(Request $request)
    {
        #Newsletter::subscribe('giojkd@gmail.com');
        $email = $request->input('email');
        $date_of_birth = $request->input('date_of_birth');
        $nationality = $request->input('nationality');
        $gender = $request->input('gender');
        $address = json_encode(['state' => 'Florence']);
        $address = 'ciao';
        #dd($request->all());
        #Newsletter::subscribePending($email, ['FNAME'=>'Giovanni', 'LNAME'=>'Orlandi']);
        $date_of_birth = '01/02/2020';
        $nationality = 'Italian';
        $gender = 'Male';
        $result = Newsletter::subscribePending($email, ['DATEOFBIRT' => $date_of_birth, 'NATIONALIT' => $nationality, 'GENDER' => $gender]);
        dd($result);
        echo 'done';

        /*$MailChimp = new MailChimp('7c96f2211d44ad649351a4f0ff80eff3-us19');
        $list_id = 'ac36ff5d2b'; #env('MAILCHIMP_LIST_ID');
        $result = $MailChimp->post("lists/$list_id/members", [
                  'email_address' => 'giojkd@gmail.com',
                  'status'        => 'subscribed',
              ]);

        dd($result);*/

    }

    public function home(Request $request)
    {

#     App::setLocale('it');

        $today = date('Y-m-d');
        $in90Days = date('Y-m-d', strtotime('+90days'));
        $data = [];
        $data['products'] = Product
            ::with([
                'geofence',
                'category',
                'availabilities' => function ($query) use ($today, $in90Days) {
                    $query->whereBetween('day', [$today, $in90Days]);
                }
            ])
            ->where('enabled', 1)
            ->orderBy('created_at', 'DESC')
            ->limit(9)
            ->get();

        $data['categories'] = Category
            ::withCount(['products'])
            ->get();

        $data['geofences'] = Geofence
            ::all();

        $data['menus'] = Menu::with(['articles' => function ($query) {
            $query->orderBy('lft', 'ASC');
        }])->get()->keyBy('slug');
        $data['textBlocks'] = Textblock::all()->pluck('content', 'slug');
        $data['slideshows'] = Slideshow::all()->keyBy('slug');

        #dd($data);
        $data['languages'] = Language::where('is_website_language', 1)->get()->keyBy('iso');
        return view('front.home', $data);
    }

    public function article($name, $id)
    {
        $data = [];
        $data['menus'] = Menu::with(['articles' => function ($query) {
            $query->orderBy('lft', 'ASC');
        }])->get()->keyBy('slug');
        $data['article'] = Article::find($id);
        $data['languages'] = Language::where('is_website_language', 1)->get()->keyBy('iso');
        return view('front.article', $data);
    }

    public function category($name, $id)
    {
        $today = date('Y-m-d');

        $in15Days = date('Y-m-d', strtotime('+15days'));
        $data = [];
        $data['menus'] = Menu::with(['articles' => function ($query) {
            $query->orderBy('lft', 'ASC');
        }])->get()->keyBy('slug');
        $data['category'] = Category::find($id);
        $data['products'] = Product
            ::with([
                'geofence',
                'category',
                'availabilities' => function ($query) use ($today, $in15Days) {
                    $query->whereBetween('day', [$today, $in15Days]);
                }
            ])
            ->whereNotNull('photos')
            ->where('category_id', $id)
            ->where('enabled', 1)
            ->limit(10)
            ->orderBy('updated_at', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->get();
        $data['languages'] = Language::where('is_website_language', 1)->get()->keyBy('iso');
        return view('front.category', $data);
    }

    public function product($name, $id)
    {
        $data = [];



        $today = date('Y-m-d');
        $in90Days = date('Y-m-d', strtotime('+365days'));
        $data = [];
        $data['menus'] = Menu::with(['articles' => function ($query) {
            $query->orderBy('lft', 'ASC');
        }])->get()->keyBy('slug');

        $data['product'] = Product
            ::with([
                'geofence',
                'addons',
                'category',
                'languages',
                'reviews',
                'pricerules',
                'availabilities' => function ($query) use ($today, $in90Days) {
                    $query->whereBetween('day', [$today, $in90Days]);
                }
            ])
            ->where('id', $id)
            ->first();




        $data['similar_products'] = Product
            ::with([
                'geofence',
                'category',
                'availabilities' => function ($query) use ($today, $in90Days) {
                    $query->whereBetween('day', [$today, $in90Days]);
                }
            ])
            ->whereNotNull('photos')
            ->where('category_id', $data['product']->category_id)
            ->where('id', '!=', $data['product']->id)
            ->limit(10)
            ->orderBy('updated_at', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->get();


        $data['languages'] = Language::where('is_website_language', 1)->get()->keyBy('iso');
        #dd($data);
        return view('front.product', $data);
    }


    public function addToCart(Request $request)
    {
        $lead_id = Cookie::get('lead_id');
        if ($lead_id != '') {
            $lead = Lead::find($lead_id);
        } else {
            $lead = Lead::create([]);
            Cookie::queue(Cookie::make('lead_id', $lead->id, $this->cookieDuration));
        }
        $product = Product::findOrFail($request->input('product_id'));
        $date = Carbon::createFromFormat('m/d/Y', $request->input('date'))->format('Y-m-d');
        $availability = Availability
            ::where('product_id', $request->input('product_id'))
            ->where('day', $date)
            ->where('time', $request->input('time') . ':00')
            ->first();
        $totParticipants = $request->input('children') + $request->input('adults');
        $vehiclesCount = ceil($totParticipants / $product->free_transfer_up_to);


        $leadrow = Leadrow
            ::firstOrCreate(
                [
                    'lead_id' => $lead->id,
                    'date' => $date,
                    'time' => $request->input('time') . ':00',
                ]
            );

        #$leadrow->addons->delete();

        $addons = $request->input('input_addons');
        $addonsTotal = 0;
        if ($addons != '') {
            $addons = explode(',', $addons);
            $addonsObjects = Productaddon::find($addons);

            foreach ($addonsObjects as $obj) {

                $addonsTotal += $obj->price_per_adult * $request->input('adults') + $obj->price_per_child * $request->input('children');
            }
        }


        $leadrow->addons()->sync($addons);

        $total =
            $availability->price_per_child * $request->input('children')
            + $availability->price_per_adult * $request->input('adults')
            + ($vehiclesCount * $product->free_transfer)
            + $addonsTotal;

        $leadrow
            ->fill([
                'product_id' => $request->input('product_id'),
                'adults' => $request->input('adults'),
                'children' => $request->input('children'),
                'price_per_adult' => $availability->price_per_adult,
                'price_per_child' => $availability->price_per_child,
                'total' => $total
            ])
            ->save();

        if ($product->interstitial_page_after_adding_to_cart) {
            return redirect(route('interstitialPageAfterAddingToCart', ['id' => $product->id]));
        }

        return redirect(route('checkout'));
    }

    public function removeFromCart($id)
    {
        $leadRow = Leadrow::findOrFail($id);
        $leadRow->delete();
        return redirect(route('checkout'));
    }


    public function calculateTransferCost(Request $request)
    {

        $lead_id = Cookie::get('lead_id');

        if ($lead_id != '') {
            $lead = Lead::find($lead_id);
        } else {
            $lead = Lead::create([]);
            Cookie::queue(Cookie::make('lead_id', $lead->id, $this->cookieDuration));
        }

        $lead->pickup_address_latitude = $request->input('pickup_address_latitude');
        $lead->pickup_address_longitude = $request->input('pickup_address_longitude');
        $lead->pickup_address = $request->input('pickup_address');
        $lead->save();


        $lead->calculateLeadTotals();

        return redirect()->route('checkout');

        exit;

    }

    public function checkout()
    {

        $lead_id = Cookie::get('lead_id');
        if ($lead_id != '') {
            $lead = Lead::find($lead_id);
        } else {
            $lead = Lead::create([]);
            Cookie::queue(Cookie::make('lead_id', $lead->id, $this->cookieDuration));
        }
        $data = [
            'lead' => $lead,
            'rows' => $lead->leadrows()->orderBy('date', 'asc')->orderBy('time', 'asc')->get()
        ];
        $data['menus'] = Menu::with(['articles' => function ($query) {
            $query->orderBy('lft', 'ASC');
        }])->get()->keyBy('slug');
        $data['languages'] = Language::where('is_website_language', 1)->get()->keyBy('iso');
        return view('front.checkout', $data);
    }

    public function chargeCard($card, $total, $client_email, $description)
    {

        #dd($card);

        \Stripe\Stripe::setApiKey(env('STRIPE_API_KEY'));

        try {
            $token = \Stripe\Token::create(
                array(
                    "card" => array(
                        "name" => $card['name'],
                        "number" => $card['num'],
                        "exp_month" => $card['mm'],
                        "exp_year" => $card['yy'],
                        "cvc" => $card['ccv']
                    )
                )
            );
            $customer = \Stripe\Customer::create([
                'source' => $token->id,
                'email' => $client_email
            ]);
            $chargePreAuth = \Stripe\Charge::create([
                "amount" => round($total * 100),
                "currency" => "eur",
                "customer" => $customer->id,
                "description" => $description,
                "capture" => false, #<---------- preautorizzazione
            ]);
            $chargeId = $chargePreAuth->id;
        } catch (\Stripe\Exception\CardException $e) {

            $error = $e->getJsonBody();
            $chargePreAuth = ['status' => 'failed','error' => $error];


        }

        return $chargePreAuth;
    }


    public function thankYou($id)
    {
        $data = [];
        $data['menus'] = Menu::with(['articles' => function ($query) {
            $query->orderBy('lft', 'ASC');
        }])->get()->keyBy('slug');
        $data['languages'] = Language::where('is_website_language', 1)->get()->keyBy('iso');
        return view('front.thank_you', $data);
    }

    public function map()
    {
        $categoryId = 6;
        $today = date('Y-m-d');

        $in15Days = date('Y-m-d', strtotime('+15days'));
        $data = [];
        $data['menus'] = Menu::with(['articles' => function ($query) {
            $query->orderBy('lft', 'ASC');
        }])->get()->keyBy('slug');
        $data['category'] = Category::find($categoryId); #where 6 is wine category
        $data['productGroups'] = Product
            ::with([
                'geofence',
                'category',
                'availabilities' => function ($query) use ($today, $in15Days) {
                    $query->whereBetween('day', [$today, $in15Days]);
                }
            ])
            ->whereNotNull('photos')
            ->where('category_id', $categoryId)
            #->limit(10)
            ->where('enabled',1)
            ->orderBy('updated_at', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->get()
            ->groupBy('geofence_id');


        $path = "https://www.google.com/maps/d/u/1/kml?forcekml=1&mid=1WZ7IXzCMpMZHoJmAZriy4zXbxzk&lid=43oqEyMN-EE";
        $kml = json_decode(json_encode(collect(simplexml_load_string(file_get_contents($path)))), true);


        $coordinates = [];
        foreach ($kml['Document']['Placemark'] as $polygon) {

            $pol = $polygon['Polygon']['outerBoundaryIs']['LinearRing']['coordinates'];

            $coordinates[Str::slug($polygon['name'])] = collect(array_filter(array_map('trim', explode(',0', $pol))))->map(function ($item, $key) {
                return explode(',', $item);
            });
        }


        $coordinates = collect($coordinates);
        $data['coordinates'] = $coordinates;
        $data['languages'] = Language::where('is_website_language', 1)->get()->keyBy('iso');
        return view('front.map', $data);
    }

    public function contactUs()
    {
        $data = [];
        $data['menus'] = Menu::with(['articles' => function ($query) {
            $query->orderBy('lft', 'ASC');
        }])->get()->keyBy('slug');
        $data['languages'] = Language::where('is_website_language', 1)->get()->keyBy('iso');
        return view('front.contact_us', $data);
    }

    public function experiences()
    {
        $today = date('Y-m-d');
        $in15Days = date('Y-m-d', strtotime('+15days'));
        $data = [];
        $data['menus'] = Menu::with(['articles' => function ($query) {
            $query->orderBy('lft', 'ASC');
        }])->get()->keyBy('slug');
        $data['productGroups'] = Product
            ::with([
                'geofence',
                'category',
                'availabilities' => function ($query) use ($today, $in15Days) {
                    $query->whereBetween('day', [$today, $in15Days]);
                }
            ])
            ->whereNotNull('photos')
            ->where('enabled', 1)
            ->orderBy('updated_at', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->get()
            ->groupBy('category_id');
        $data['languages'] = Language::where('is_website_language', 1)->get()->keyBy('iso');
        return view('front.experiences', $data);
    }

    public function setLanguage($lang)
    {
        \Session::put('locale', $lang);
        return redirect()->back();
    }

    public function vouchers()
    {
        $data = [];
        $data['menus'] = Menu::with(['articles' => function ($query) {
            $query->orderBy('lft', 'ASC');
        }])->get()->keyBy('slug');
        $data['languages'] = Language::where('is_website_language', 1)->get()->keyBy('iso');
        return view('front.vouchers', $data);
    }

    public function pay(Request $request)
    {

        $validator = Validator::make(
                $request->all(), [
                'client_name' => 'required',
                'client_surname' => 'required',
                'client_telephone' => 'required',
                'client_email' => 'required|email',
                'client_address' => 'required',
                'pickup_address' => 'required',
                'pickup_vehicle' => 'nullable|date',
                'card.name' => 'required',
                'card.num' => 'required',
                'card.mm' => 'required',
                'card.yy' => 'required',
                'card.ccv' => 'required',
                'accept_terms_and_conditions' => 'required'
        ]);

        if ($validator->fails()) {
            return
                redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $lead_id = Cookie::get('lead_id');
        if ($lead_id != '') {
            $lead = Lead::find($lead_id);
        } else {
            return redirect(route('home'));
        }

        $validator->after(function ($validator) use ($request,$lead)  {
            $chargePreAuth = $this->chargeCard($request->input('card'), $lead->grand_total, $request->input('client_email'), "TalixoTuscany " . $lead->id);
            if (isset($chargePreAuth['error'])) {
                #dd($chargePreAuth['error']);
                #return redirect()->back()->withInput($chargePreAuth['error']);
                $validator->errors()->add('card.num', $chargePreAuth['error']['error']['message']);
            }

        });

        if ($validator->fails()) {
            return
            redirect()
            ->back()
            ->withErrors($validator)
            ->withInput();
        }






        if ($chargePreAuth['status'] == 'succeeded') {

            $lead->fill([
                'client_name' => $request->input('client_name'),
                'client_surname' => $request->input('client_surname'),
                'client_address' => $request->input('client_address'),
                'client_email' => $request->input('client_email'),
                'client_telephone' => $request->input('client_telephone'),
                'notes' => $request->input('notes'),
                'payment_method' => $request->input('payment_method'),
                'pickup_address' => $request->input('pickup_address'),
                'pickup_vehicle' => $request->input('pickup_vehicle')
            ])
                ->save();

            $lead->sendConfirmation();
            return redirect()->route('thankYou', ['id' => $lead->id]);
        }


    }

    public function voucherCheckout(Request $request)
    {
        #dd($request->all());
        $request->validate([
            'client_name' => 'required',
            'client_surname' => 'required',
            'client_telephone' => 'required',
            'client_email' => 'required|email',
            'client_address' => 'required',
            'card.num' => 'required',
            'card.name' => 'required',
            'card.mm' => 'required',
            'card.yy' => 'required',
            'card.ccv' => 'required',
        ]);

        $voucher = Voucher::create([
            'client_name' => $request->input('client_name'),
            'client_surname' => $request->input('client_surname'),
            'client_address' => $request->input('client_address'),
            'client_email' => $request->input('client_email'),
            'client_telephone' => $request->input('client_telephone'),
            'notes' => $request->input('notes'),
            'payment_method' => $request->input('payment_method'),
            'grand_total' => $request->input('grand_total')
        ]);

        $chargePreAuth = $this->chargeCard($request->input('card'), $request->input('grand_total'), $request->input('client_email'), "TalixoTuscany Voucher " . $voucher->id);

        if ($chargePreAuth['status'] == 'succeeded') {
            $voucher->generateCode();
            $voucher->sendConfirmation();
            return redirect()->route('thankYou', ['id' => $voucher->id]);
        }


    }

    public function interstitialPageAfterAddingToCart($id)
    {

        $today = date('Y-m-d');
        $in90Days = date('Y-m-d', strtotime('+90days'));

        $product = Product::findOrFail($id);

        $data = [];
        $data['added_product'] = $product;
        $data['products'] = Product
            ::with([
                'geofence',
                'category',
                'availabilities' => function ($query) use ($today, $in90Days) {
                    $query->whereBetween('day', [$today, $in90Days]);
                }
            ])
            ->whereNotNull('photos')
            ->where('category_id', $product->category_id)
            ->where('id', '!=', $product->id)
            ->limit(10)
            ->orderBy('updated_at', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->get();


        $data['menus'] = Menu::with(['articles' => function ($query) {
            $query->orderBy('lft', 'ASC');
        }])->get()->keyBy('slug');
        $data['languages'] = Language::where('is_website_language', 1)->get()->keyBy('iso');
        return view('front.interstitial_page', $data);
    }

}
