<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use App\Traits\Helpers;
use App\Models\Product;
use App\Models\Leadrow;
use App\Observers\ProductObserver;
use App\Observers\LeadrowObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Product::observe(ProductObserver::class);
        Leadrow::observe(LeadrowObserver::class);
    }
}
