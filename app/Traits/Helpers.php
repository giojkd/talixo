<?php
namespace App\Traits;

trait Helpers
{

    public function time_to_decimal($time) {
        $timeArr = explode(':', $time);
        $decTime = ($timeArr[0]*60) + ($timeArr[1]) + ($timeArr[2]/60);

        return $decTime;
    }

    function addZ($value){
      if((float)$value < 10)
        return '0'.$value;
      return $value;
    }

    function decimal_to_time($decimal) {
        $hours = floor((int)$decimal / 60);
        $minutes = floor((int)$decimal % 60);
        $seconds = $decimal - (int)$decimal;
        $seconds = round($seconds * 60);

        return str_pad($hours, 2, "0", STR_PAD_LEFT) . ":" . str_pad($minutes, 2, "0", STR_PAD_LEFT) . ":" . str_pad($seconds, 2, "0", STR_PAD_LEFT);
    }

    public function calculateLeadTotals()
    {
        $lead = $this;

        $lead->calculateLeadTransferPrice();

        $leadrows = $lead->leadrows;

        if (!is_null($leadrows) || $leadrows->count() == 0) {
            $lead->products_price = $leadrows->sum('total');
        } else {
            $lead->products_price = 0;

        }
        $lead->grand_total = $lead->products_price + $lead->transfer_price;
        $lead->save();
    }

    public function calculateLeadTransferPrice()
    {

        $transportQuotation = $this->calculateTransportationCost();


        $includedFreeTransfer = 0;

        foreach ($this->leadrows as $row) {
            $product = $row->product;
            $participants = $row->adults + $row->children;
            $freeTransferSoil = ceil($participants / $product->free_transfer_up_to);
            $includedFreeTransfer += $freeTransferSoil * $product->free_transfer;
        }

        if ($transportQuotation['status'] == 1) {

            $this->pickup_address = $transportQuotation['segments'][0]['from'];
            $this->transfer_quotation = $transportQuotation;
            $this->transfer_price = $transportQuotation['price'] - $includedFreeTransfer;
            $this->transfer_km = $transportQuotation['totals']['meters'] / 1000;
            $this->grand_total = $this->transfer_price + $this->products_price;
            $this->transfer_disposal = $this->decimal_to_time($transportQuotation['disposalDuration']);
            $this->save();

        } else {

        }
    }

    public function timeToMinutes($time)
    {
        $tmp = explode(':', $time);
        $minutes = (int)$tmp[0] * 60;
        $minutes += (int)$tmp[1];
        $minutes += ((int)$tmp[2] > 30) ? 1 : 0;
        return $minutes;
    }

    static function calculateDistanceBetweenTwoPoints($from, $to, $cfg = [])
    {
        $from = implode(',', (array)$from);
        $to = implode(',', (array)$to);

#        dd(func_get_args());

        $client = new \GuzzleHttp\Client([
            'base_uri' => 'https://maps.googleapis.com'
        ]);
        $method = 'maps/api/distancematrix/json';
        $res = $client->request('GET', $method, [
            'query' => [
                'units' => 'metric',
                'origins' => $from,
                'destinations' => $to,
                'language' => 'it-IT',
                'mode' => 'driving',
                #'traffic_model' => 'best_guess',
                'key' => env('GOOGLE_API_KEY_UNRESTRICTED')
            ]
        ]);
        $res = json_decode($res->getBody());

                #dd($res);

        (object)$return = [
            'to' => $res->destination_addresses[0],
            'from' => $res->origin_addresses[0],
            'distance' => $res->rows[0]->elements[0]->distance->value,
            'duration' => $res->rows[0]->elements[0]->duration->value,
        ];

        #$res = ;
        return $return;
    }

    function makePermalink($str, $cfg = [])
    {
        #$str = trim($cfg['str']);
        $replace = isset($cfg['replace']) ? $cfg['replace'] : array();
        $delimiter = isset($cfg['delimiter']) ? $cfg['delimiter'] : '-';

        if (!empty($replace)) {
            $str = str_replace((array)$replace, ' ', $str);
        }

        $clean = preg_replace("/[^a-zA-Z0-9\/_.|+ -]/", '', $str);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\_|+ -]+/", $delimiter, $clean);

        return $clean;
    }

    function getUrl($routeName)
    {
        return route($routeName, ['id' => $this->id, 'name' => $this->makePermalink($this->name)]);
    }

    public function saveCoordinates()
    {
        $address = json_decode($this->address);
        $this->longitude = $address->latlng->lng;
        $this->latitude = $address->latlng->lat;
        $this->saveWithoutEvents();
    }

    public function saveWithoutEvents(array $options = [])
    {
        return static::withoutEvents(function () use ($options) {
            return $this->save($options);
        });
    }


}


?>
